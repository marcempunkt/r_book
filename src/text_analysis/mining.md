# Text Mining

## Was ist Text Mining?

1. Text lesen
2. Text reinigen
3. Text analysieren

## Text lesen

Als aller erstes muessen wir das `tm` Package installlieren und 
dann in unseren Script importieren.

```R
install.packages("tm")
library(tm)
```

Mit `readLines` können wir eine Textdatei lesen:

```R
text <- readLines("path/to/file")
```

Der Funktion `readLines` müssen wir den Pfad zur unserer Textdatei geben.
Dabei ist der Pfad relativ, dies heißt, dass wir nicht den kompletten Pfad
mit `C:\\\\...` oder so angeben müssen, sondern nur den restlichen Pfad ab
dem Beginn des Projektordners.  
In anderen Worten, wenn unsere Datei in einem Ordner "books" liegt,
dann ist der Pfad `books/<Dateiname>.txt`. 

In R werden Ordner innerhalb eines Pfades mit einem `/` gekennzeichnet.

`readLines` gibt uns einen Textvektor wieder, 
wo jedes Element einer Zeile in dem gelesenen Textdokument entspricht.

Anstelle einen Pfad angeben zu müssen, können wir auch die Funktion `file.choose` verwenden.
Diese öffnet jeh nach Platform einen Filechooser Dialog, 
wo wir dann die Datei aussuchen können.

```R
text <- readLines(file.choose())
```

Um zu verstehen wie `readLines` funktioniert,
sind hier die ersten Zeilen unserer `text` Variable,
die wir mit Hilfe der `head` Funktion bekommen.

```R
head(text)
```

```
Output
```

Jetzt haben wir unseren Text gelesen und in der Variable `text` abgespeichert,
aber arbeiten können wir damit noch nicht.
Bevor wir aber mit dem nächsten Schritt "Reinigung" anfangen können,
müssen wir unserer `text` Variable von einem Textvektor zu einem `Corpus` umkonvertieren.

```R
text <- readLines("./path/to/file.txt")
text <- VectorSource(text)
text <- Corpus(text)
```

Hier konvertieren `text` erstmal zu einem `VectorSource`.
`VectorSource` ist ein spezielles Objekt vom `tm` Package der, 
den Rohtext nimmt und in einer Datenstruktur packt,
die es einfacher macht den gelesenen Text in ein neuen Datentyp, 
den `Corpus`, auch vom `tm` Package, zu konvertieren.
Dies müssen wir machen, weil es die weitere Arbeit für uns einfacher machen wird.

Aber was ist überhaupt ein __Corpus__?

Ein Corpus ist ein Objekt, welches mehrere Dokumente auf einmal in sich speichern kann.

In den nächsten Schritten werden wir jetzt den gelesenen Text weiter aufräumen,
um ungewünschte Zeichen wie Punkt, Komma, Zahlen, Links etc. zu entfernen.
Da ein Corpus auch mehrere Texte (Buecher, Artikel etc.) aufeinmal in sich tragen kann,
können wir all diese Prozesse auf ein `Corpus` Objekt ausführen,
anstelle es für jeden einzelnen Text einzelnd machen zu müssen.

Für die Bereinigung unseres Textes bietet das `tm` Package mehrere Funktionen an,
die perfekt auf ein `Corpus` Objekt angewendet werden können.

## Text aufräumen

Jetzt können wir anfangen den Textcorpus weiter zu aufzuräumen:

```R
text <- tm_map(text, content_transformer(tolower))
text <- tm_map(text, stripWhitespace)
text <- tm_map(text, removePunctuation)
text <- tm_map(text, removeNumbers)
text <- tm_map(text, removeWords, stopwords("english"))
```

TODO was macht `tm_map` und `content_transformer`.

Zur guter letzt konvertieren jedes Dokument, 
welches in unserem `Corpus` gespeichert worden ist, 
zu einem `TextDocument` Objekt mit der Funktion `PlainTextDocument`.
Dies muessen wir machen, weil in einem `Corpus` nicht nur 
reine `.txt` Dateien gespeichert werden koennen,
sondern auch `.html` oder `.pdf` Dateien 
und diese wollen wir als normale Textdokumente haben.

```R
text <- tm_map(text, PlainTextDocument)
```

Als letzter Aufräumschritt fehlt nur noch das `stemming`.
Fuer das stemming benutzen wir das `SnowballC` package,
welches sich perfekt für englische Texte anbietet.

Als erstes müssen wir dafuer `SnowballC` installieren und importieren.

```R
install.packages("SnowballC")
library(SnowballC)
```

```R
tm_map(text, stemDocument)
```

### Visualisierung

als erstes muss zu einer termdocumentmatrix umgewandelt werden

```R
text_matrix <- TermDocumentMatrix(text)
## text_matrix <- DocumentTermMatrix(text)
text_matrix <- as.matrix(text_matrix)
```

```R
text_matrix <- sort(rowSums(text_matrix), decreasing=T)
```

```R
text_df <- data.frame(word=names(text_matrix), freq=text_matrix)
```

#### Worldcloud

```R
set.seed(1234)
wordcloud(words = text_df$word, 
          freq = text_df$freq, 
          min.freq = 1,
          max.words=200, 
          random.order=FALSE, 
          rot.per=0.35,
          color=brewer.pal(1, "Paired"))
```

#### ggplot n-bars





















### DirSource vs. VectorSource

oder DirSource aber dafuer gibt man der `DirSource` Funktion den Pfad zum Ordner, den wir gelesen haben wollen.

```R
text <- readLines("./src/SUMMARY.md")
text <- DirSource(text)
text
```

Jetzt muessen wir den text von einem VectorSource zu einem Corpus umkonvertieren,
damit wir daraufhin den Text bereinigen koennen.

```R
text <- readLines("./path/to/file")
text <- VectorSource(text)
inspect(text)
```
