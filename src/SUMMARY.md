# Summary

[Willkommen](../README.md)

# R und Rstudio

- [Installation](r_und_rstudio/installation.md)
- [Einführung in Rstudio](r_und_rstudio/einfuehrung_in_rstudio.md)
- [Einführung in R Interpreter](r_und_rstudio/einfuehrung_in_r_interpreter.md)

# Grundlagen

- [Variablen](grundlagen/variablen.md)
- [Datentypen](grundlagen/daten_typen/index.md)
    - [Number](grundlagen/daten_typen/number.md)
    - [Boolean](grundlagen/daten_typen/boolean.md)
        - [Logische Operatoren](grundlagen/daten_typen/logische_operatoren.md)
        - [if und else](grundlagen/daten_typen/if_und_else.md)
        - [switch](grundlagen/daten_typen/switch.md)
    - [Character](grundlagen/daten_typen/character_und_string.md)
    - [Vector](grundlagen/daten_typen/vector.md)
        - [Indexing](grundlagen/daten_typen/vector_indexing.md)
        - [Mathe mit Vektoren](grundlagen/daten_typen/vector_maths.md)
        - [Filtering](grundlagen/daten_typen/vector_filtering.md)
        - [Named Vectors](grundlagen/daten_typen/vector_named.md)
        - [Übungsaufgaben](grundlagen/daten_typen/vector_exercises.md)
    - [Matrix](grundlagen/daten_typen/matrix.md)
        - [Matrix erweitern](grundlagen/daten_typen/matrix_bind.md)
        - [Named Matrices](grundlagen/daten_typen/matrix_named.md)
        - [Indexing](grundlagen/daten_typen/matrix_indexing.md)
        - [Mathe mit Matrizen](grundlagen/daten_typen/matrix_maths.md)
        - [Filtering](grundlagen/daten_typen/matrix_filtering.md)
        - [Übungsaufgaben](grundlagen/daten_typen/matrix_exercises.md)
    - [List](grundlagen/daten_typen/list.md)
        - [Indexing](grundlagen/daten_typen/list_indexing.md)
        - [Named Lists](grundlagen/daten_typen/list_named.md)
        - [Übungsaufgaben](grundlagen/daten_typen/list_exercises.md)
    - [Factor](grundlagen/daten_typen/factor.md)
        - [Ordered Factor](grundlagen/daten_typen/factor_ordered.md)
    - [DataFrame](grundlagen/daten_typen/dataframe.md)
        - [Indexing](grundlagen/daten_typen/dataframe_indexing.md)
        - [DataFrame erweitern](grundlagen/daten_typen/dataframe_bind.md)
        - [Wichtige Funktionen](grundlagen/daten_typen/dataframe_functions.md)
- [Schleifen](grundlagen/schleifen/index.md)
    - [for](grundlagen/schleifen/for.md)
        - [mit Vektoren](grundlagen/schleifen/for_vector.md)
        - [mit Listen](grundlagen/schleifen/for_list.md)
        - [mit Matrizen](grundlagen/schleifen/for_matrix.md)
        - [Übungsaufgaben](grundlagen/schleifen/for_exercises.md)
    - [while](grundlagen/schleifen/while.md)
    - [repeat](grundlagen/schleifen/repeat.md)
- [Funktionen](grundlagen/funktionen/index.md)
    - [Eigene Funktionen schreiben](grundlagen/funktionen/function_own.md)
    - [Named Parameters](grundlagen/funktionen/function_named_params.md)
    - [return](grundlagen/funktionen/function_return.md)
    - [Übungsaufgaben](grundlagen/funktionen/function_exercise.md)

# Plots generieren

- [plot](plots/index.md)
    - [mehrere Plots in einem Plot](plots/many_plots.md)
    - [Legende im Plot](plots/legend.md)
    - [Plots speichern](plots/write_to_fs.md)
    - [Übungsaufgaben](plots/plot_exercise.md)
- [ggplot](plots/ggplot.md)

# Textanalyse

- [Text Mining](text_analysis/mining.md)

# Bonuskapitel

- [gganimate]()
- [plotly]()

-----------
[Kontributionen](contributors.md)
