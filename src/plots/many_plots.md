# mehrere Plots in einem Plot

Manchmal wollen wir mehrere Plots in einem Plot haben.

- [mehrere Linien in einem Koordinatensystem](#mehrere-linien-in-einem-plot)
- [mehrere Datensets in Form von Punkten in einem Plot](#mehrere-punkte-in-einem-plot)
- [mehrere Koordinatensystem mit deren eigenen Linien, Punkten und Styles](#mehrere-verschiedene-plots-aufeinmal)

Diese 3. verschiedenen Möglichkeiten werden wir in diesem Kapitel lernen!

## mehrere Linien in einem Plot

Das hier ist unser Anfangsplot, wie vom vorherigen Kapitel, die Funktion `y=x^2`:

```r
x <- 1:10
plot(x,
     x^2,
     type="l")
```

<div class="flex-center">
    <img src="/assets/plots/11.jpeg" alt="plots/11" />
</div>

Um eine weitere Linie, zum Beispiel die Funktion `y=x^3`, hinzuzufügen, 
müssen wir nur, nachdem wir die  `plot` Funktion benutzt haben, die `lines` Funktion aufrufen.

Die `lines` Funktion hat die selben Parameter wie die `plot` Funktion, 
aber nur die, die wichtig sind um eine Linie darzustellen und zu stylen.

```r
x <- 1:10
plot(x,
     x^2,
     type="l",
     lwd=3)
     
lines(x,
      x^3,
      col="gray",
      lwd=3)
```

<div class="flex-center">
    <img src="/assets/plots/12.jpeg" alt="plots/12" />
</div>

Wir können so viele Linien hinzufügen wie wir wollen:

```r
x <- 1:10
plot(x,
     x^2,
     type="l",
     lwd=3)
     
lines(x,
      x^3,
      col="gray",
      lwd=3)
      
lines(x,
      x,
      col="red",
      lwd=3)
```

<div class="flex-center">
    <img src="/assets/plots/13.jpeg" alt="plots/13" />
</div>

Wie dir aber vielleicht aufgefallen ist, 
wird unser Koordinatensystem nicht für die neuen Linien angepasst.
Dies liegt daran, dass die `lines` Funktion die neuen Linien einfach
in das vorhandene Koordinatensystem reinmalt ohne dieses zu verändern.
Im Kapitel [ggplot]() werden wir lernen, wie wir noch viel besser und hübschere Plots erstellen können.

## mehrere Punkte in einem Plot

TODO erstelle zwei Fakedatasets mit Leuten die "gesund" oder "ungesund" leben und in welchem Alter sie sterben.

Um noch mehr Punkte zu einem Plot hinzuzufügen, brauchen wir die `points` Funktion.
Die `points` Funktion verhält sich im Grunde genau so wie `lines`, nur wieder hier, 
dass man nur die Parameter verwenden kann, die man braucht um Punkte zu stylen.

```r
healthy = sample(60:100, 100, replace=TRUE)
unhealthy = sample(40:90, 100, replace=TRUE)
```

```r
plot()

points()
```

## mehrere verschiedene Plots aufeinmal

Mehrere Koordinatensystem mit deren eigenen Linien in einem generierten Bild? Mega easy!

Die `par` Funktion mit dem `mfrow` Parameter um ein Grid zu erstellen, 
dabei wird mit einem Vektor die Zeilen und die Spalten, in dieser Reihenfolge auch, definiert.

```r
par(mfrow=c(2,2))

x <- 1:100
plot(x,
     x,
     main="y=x",
     type="l")

plot(x,
     x^2,
     main="y=x^2",
     type="l")

plot(x,
     x^3,
     main="y=x^3",
     type="l")

plot(x,
     x^6,
     main="y=x^6",
     type="l")
     
dev.off()
```

<div class="flex-center">
    <img src="/assets/plots/14.jpeg" alt="plots/14" />
</div>

Nach `par` können wir anfangen unsere Plots zu generieren und 
diese werden dann auch in dieser Reihe in das Grid eingesetzt, 
welches wir mit `mfrow` definiert haben.

Dir ist bestimmt das `dev.off()` am Ende aufgefallen, oder?
`dev.off()` resetet alles, was die Renderengine der Plotgenerierung beeinflussen könnte.
Dies ist wichtig, sodass wir mögliche Bugs später im Code verhindern können, 
denn hunderte Zeilen später wird wohl keiner mehr wissen, 
warum sein neu generierter Plot nur noch 1/4 so groß ist wie sonst.

<!--
## notes

```r
head(airmiles)
plot(airmiles, main="Annual Air Miles Flown in the USA", xlab="Year", ylab="Miles (in billions)")
data(airmiles)


head(mtcars)
## mpg: Miles/(US) gallon
## cyl: Number of cylinders
## disp: Displacement (cu.in.)
## hp: Gross horsepower
## drat: Rear axle ratio
## wt: Weight (1000 lbs)
## qsec: 1/4 mile time
## vs: V/S
## am: Transmission (0 = automatic, 1 = manual)
## gear: Number of forward gears
## carb: Number of carburetors

head(USArrests)
## Murder: Murder arrests (per 100,000)
## Assault: Assault arrests (per 100,000)
## UrbanPop: Percent urban population
## Rape: Rape arrests (per 100,000)

head(ToothGrowth)
## len: Tooth length
## supp: Supplement type (VC or OJ).
## dose: numeric Dose in milligrams/day

head(faithful)
head(iris)
## head(mtcars)
head(trees)
head(women)
```
-->
