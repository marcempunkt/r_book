# Plots speichern

bmp, jpeg, png, tiff, pdf, ps

### jpeg

```r
jpeg(file="path/filename", 
     width=x, 
     height=y, 
     units="px", 
     bg="white",
     quality=100)

plot()

dev.off()
```

### png

```r
png(file="path/filename", 
    width=x, 
    height=y, 
    units="px", 
    bg="white",
    quality=100)

plot()

dev.off()
```


### pdf

```r
pdf(file="",
    width="", # der graphik
    height="",
    paper="a4",
    title="") # "letter", etc.
```
