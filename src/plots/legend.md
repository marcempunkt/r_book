# Legende im Plot

Wenn wir uns diesen Plot hier anschauen:

```r
x <- 1:10
plot(x,
     x^2,
     type="l",
     lwd=3)
     
lines(x,
      x^3,
      col="gray",
      lwd=3)
      
lines(x,
      x,
      col="red",
      lwd=3)
```

<div class="flex-center">
    <img src="/assets/plots/15.jpeg" alt="plots/15" />
</div>

Dann kann man vielleicht ein Problem erkennen, oder?
Man kann nicht wirklich nachvollziehen welche Linie was ist.
Wir brauchen eine Legende!

```r
legend()
```

```r
x <- 1:10
plot(x,
     x^2,
     type="l",
     lwd=3)
     
lines(x,
      x^3,
      col="gray",
      lwd=3)
      
lines(x,
      x,
      col="red",
      lwd=3)
```

<div class="flex-center">
    <img src="/assets/plots/16.jpeg" alt="plots/16" />
</div>
