# Übungsaufgaben

1. Erstelle seperate Plots für jeweils den Funktionen `y=2x`, `y=x^2 + 10` und `y=x^5`

2. Packe alle 3 seperate Plots von der ersten Aufgabe in einem einzigen Plot. 
Mit verschiedenen Farben für die Funktionen und einer passenden Legende.

3. Die 3 Funktionen der ersten Aufgaben sollen in verschiedenen Koordinatensystem 
und einem Grid von 3 Zeilen und 1 Spalte angezeigt werden.

4. Speicher den Plot von der letzten Aufgabe als jpeg und pdf ab.

