# plot
<!--
Convert plot into jpeg command:
jpeg(file="src/assets/plots/01.jpeg", width=500, height=500, units="px", quality=100)
plot()
dev.off()
-->
Herzlichen Glückwunsch, wenn du es bis hier in geschafft hast, 
dann hast so gut wie alles gelernt was man braucht,
um sich Programmierer nennen zu dürfen.

Wir fangen jetzt erst an mit dem richtig coolen Stuff, glaub mir ;)

Mit der `plot` Funktion kann man ganz easy Grafen erstellen.
Weißte noch die Dinger, die man damals in der Schule immer selber malen musste?
Japp, die können wir ganz easy generieren lassen und 
zu unseren Vorstellungen designen.

### x und y

Die `plot` Funktion braucht nur zwei Sachen, um zu funktioneren:
Und zwar Werte für jeweils der x- und y-Achse.

```r
plot(c(1,2,3,4,5),
     c(1,4,9,16,25))
```

<div class="flex-center">
    <img src="/assets/plots/01.jpeg" alt="plots/01" />
</div>

oder auch mit `named arguments`:

```r
plot(x=c(1,2,3,4,5),
     y=c(1,4,9,16,25))
```

Cool, oder? Spiel mit den Werten, um ein besseres Gefühl für das "plotten" zu bekommen.
Im Codeabschnitt von oben haben wir die mathematische Funktion `y = x^2` erstellt,
aber wir können dies auch "mathematischer" und mit weniger Code erstellen:

```r
x <- 1:10
plot(x=x,
     y=x^2)
```

<div class="flex-center">
    <img src="/assets/plots/01.jpeg" alt="plots/01" />
</div>

Unser Datenset kann beliebig groß sein, aber pass auf!
Je nachdem wie langsam oder alt dein Computer ist, kann diese Operation länger dauern
und dein Computer in eine Heizung umwandeln, 
aber vielleicht ist das auch das Feature, welches du programmieren möchtest, dann tobe dich aus!

```r
x <- 1:1000
plot(x=x,
     y=x^2)
```

<div class="flex-center">
    <img src="/assets/plots/02.jpeg" alt="plots/02" />
</div>

## Plot beschriften

Damit man unser Grafen auch verstehen kann, können wir ihm einen Titel geben.
Der Parameter dafür ist `main` und mit `sub` können wir der generierten Grafik auch einen Untertitel geben.

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2")
```


<div class="flex-center">
    <img src="/assets/plots/03.jpeg" alt="plots/03" />
</div>

`plot` tut automatisch den Ausdruck nach `x` und `y` als String nehmen,
um damit die x und y Achse zu beschriften, 
aber wir können auch mit den Parametern `ylab` und `xlab` selber bestimmen,
wie wir die Achsen beschriftet haben wollen:

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2",
     ylab="Y Achse",
     xlab="X Achse")
```

<div class="flex-center">
    <img src="/assets/plots/04.jpeg" alt="plots/04" />
</div>

## type

Wir brauchen aber keine 1000 Punkte um eine schöne Linie in unseren Grafen zu bekommen.

Die `plot` Funktion erlaubt es uns mit dem `type` Parameter 
zu bestimmen wie wir die Punkte miteinander verbindet haben wollen.
Mit `type="l"` können wir der `plot` Funktion sagen, 
dass sie alle Punkte mit einer Linie verbinden soll.

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2",
     ylab="Y Achse",
     xlab="X Achse",
     type="l")
```

<div class="flex-center">
    <img src="/assets/plots/05.jpeg" alt="plots/05" />
</div>

Hier ist eine Liste aller möglichen Werte, die wir als type an die `plot` Funktion weiter 
geben können.

|   |                         |
|---|-------------------------|
| p | Punkte                  |
| l | Linien                  |
| c | Linien mit Lücken       |
| b | Punkten mit Linien      |
| o | Punkte mit Linie drüber |
| s | Stufen                  |
| h | Histogram Style         |
| n | nichts                  |

Spiele mit diesen Werten um ein Gefühl zu bekommen, was alles möglich ist!

<div class="flex-center">
    <img src="/assets/plots/06.jpeg" alt="plots/06" />
</div>

## col

Mit dem `col` Parameter können wir sagen welche Farbe wir für die Linie und Punkte unseren Grafen geben möchten.
Um zu sehen welche Farben uns in R zur Verfügung stehen, können wir mal die `colors` Funktion aufrufen.

```r
colors()
```

```
  [1] "white"                "aliceblue"            "antiquewhite"         "antiquewhite1"        "antiquewhite2"       
  [6] "antiquewhite3"        "antiquewhite4"        "aquamarine"           "aquamarine1"          "aquamarine2"         
 [11] "aquamarine3"          "aquamarine4"          "azure"                "azure1"               "azure2"              
 [16] "azure3"               "azure4"               "beige"                "bisque"               "bisque1"             
 [21] "bisque2"              "bisque3"              "bisque4"              "black"                "blanchedalmond"      
 [26] "blue"                 "blue1"                "blue2"                "blue3"                "blue4"               
 [31] "blueviolet"           "brown"                "brown1"               "brown2"               "brown3"              
 [36] "brown4"               "burlywood"            "burlywood1"           "burlywood2"           "burlywood3"          
 [41] "burlywood4"           "cadetblue"            "cadetblue1"           "cadetblue2"           "cadetblue3"          
 [46] "cadetblue4"           "chartreuse"           "chartreuse1"          "chartreuse2"          "chartreuse3"         
 [51] "chartreuse4"          "chocolate"            "chocolate1"           "chocolate2"           "chocolate3"          
 [56] "chocolate4"           "coral"                "coral1"               "coral2"               "coral3"              

Und noch viele viele mehr...
```

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2",
     ylab="Y Achse",
     xlab="X Achse",
     type="l",
     col="pink")
```

<div class="flex-center">
    <img src="/assets/plots/07.jpeg" alt="plots/07" />
</div>

Aber man kann nicht nur die Farbe der Punkte und Linien selber verändern, 
sondern wir können alle Fraben in unserem Plot anpassen!

|          |                                         |
|----------|-----------------------------------------|
| col      | Symbolfarbe                             |
| col.main | Fontfarbe für den Titel                 |
| col.sub  | Fontfarbe für den Untertitel            |
| col.lab  | Farbe für die label der X und Y Achse   |
| col.axis | Farbe für die Achsen                    |
| fg       | Vordergrundfarbe für die Box des Grafen |

TODO freaky plot pic

<div class="flex-center">
    <img src="/assets/plots/08.jpeg" alt="plots/08" />
</div>

Wir können aber auch jede mögliche Farbe verwenden, 
selbst wenn diese nicht in R vordefiniert existieren sollte. 
Man kann den Computer auch Farben in Form eines Hexadecimal geben.
Zum Beispiel die Farbe weiß wäre als Hexadecimal `#FFFFFF` oder schwarz wäre `#000000`.

Wie der Hexadecimal-Farben-Code aufgebaut ist, würde den Rahmen dieses Buches sprängen
und ist auch total irrelevant, aber wenn es mal eine Farbe gibt, die du unbedingt
verwenden möchtest, dann gibt es hunderte Online-Tools, die dir dabei helfen,
den Code für deine Farbe heraus zu spucken.

## Punkte eines Plots hübsch machen

Mit `pch` können wir das Zeichen für einen Punkt ("Point Character") in unserem Plot ändern
und mit `cex` können wir die Größe der Punkte anpassen. 
`cex=2` würde jeden Punkt in unserem Plot doppelt so groß machen, `cex=3` dreifach und so weiter.

`pch` kann einen Wert von 0 bis 25 als `number` haben.

<div class="flex-center">
    <img src="/assets/plots/pch.jpeg" alt="plots/pch" />
</div>

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2",
     ylab="Y Achse",
     xlab="X Achse",
     type="p",
     pch=0,
     cex=5)
```

<div class="flex-center">
    <img src="/assets/plots/09.jpeg" alt="plots/9" />
</div>

## Linien eines Plots hübsch machen

Dasselbe wie bei Punkten können wir auch mit der Linie eines Plottes machen.
Mit `lty` bestimmen wir die Art einer Linie und mit `lwd` die Dicke in Pixel.

`lty` kann den Wert von 0 bis 6 als `number` haben.

<div class="flex-center">
    <img src="/assets/plots/lty.jpeg" alt="plots/lty" />
</div>

```r
x <- 1:10
plot(x=x,
     y=x^2,
     main="Maintitle: Quadratische Funktion",
     sub="Subtitle: y=x^2",
     ylab="Y Achse",
     xlab="X Achse",
     type="l",
     lty=4,
     lwd=5)
```

<div class="flex-center">
    <img src="/assets/plots/10.jpeg" alt="plots/10" />
</div>
