# Funktionen

Wir haben im Laufe dieses Buches schon viele verschiedene Funktionen benutzt.
Die `print` Funktion gibt eine Zeile Text in der Konsole aus,
die `list` Funktion erstellt eine neue Liste,
die `matrix` Funktion erstellt eine neue Matrix,
die `c` Funktion, um mehrere Werte zu einem Vektor zu kombinieren, und und und... 
Ich glaube du verstehst was ich meine.

In diesem Kapitel werden wir lernen, wie wir unsere eigene Funktionen schreiben können.
Durch eigene Funktionen können wir unser Code in wiederverwendbare logische Blöcke einteilen,
um somit unseren Code modularer aber auch verständlicher zu machen. 
