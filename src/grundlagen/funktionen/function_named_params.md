# Named Parameters (Benannte Parameter)

Als wir Matrizen in [Kapitel 5.5.](../daten_typen/matrix.md) gelernt haben, 
haben wir auch zum aller ersten Mal benannte Parameter kennengelernt.

Schauen wir uns nochmal unsere `hello` Funktion an:

```r
hello <- function(name = "stranger") {
    print(paste("Hello", name))
}
```

Wenn wir die Funktion aufgerufen haben, dann mussten wir `hello("marc")` oder ähnliches eintippen.
In der Funktionsdefinition `function(name = "stranger")` sagen wir, wie der Funktionsparameter
der `hello` Funktion lautet und so können innerhalb des Funktionskörpers darauf zugreifen.
Aber wir können auch beim Funktionsaufruf den Namen der Parameter verwenden:

```r
hello(name = "Marc")
```

Bei mehr als einem Parameter macht es durchaus Sinn diese Syntax zu bevorzugen,
da sie zur mehr Klahrheit in unseren Code führt, z.B. bei einer `hello` Funktion,
die ein Begrüßung und einen Namen erwartet:

```r
hello <- function(greeting = "Hallo", name = "stranger") {
    print(paste(greeting, name))
}

hello(greeting="Hi", name="Marc")
```

```
[1] "Hi Marc"
```


## Die Reihenfolge der Parameter ist wichtig!

Wenn wir eine Funktion "callen" (coole Programmierersprache für "aufrufen"),
dann ist die Reihenfolge, in welcher wir die Parameter innerhalb der `()` Klammern schreiben,
wichtig.

Unsere `hello` Funktion nimmt zwei Parameter; `greeting` und `name`:

```r
hello("hi", "marc")
```

```
[1] "hi marc"
```

Wenn wir die Reihenfolge vertauschen, dann werden auch die Werte den falschen Parametern überwiesen:

```r
hello("marc", "hi")
```

```
[1] "marc hi"
```

R kann nicht für uns automatisch erkennen, was ein "name" und was ein "greeting" ist.
Aber mit *Named Parameter*, können wir R genau sagen, welchen Wert wir welchen Parameter zuweisen
wollen und dann ist auch die Reihenfolge komplett egal:

```r
hello(name="marc", greeting="hi")
```

```
[1] "hi marc"
```

Aber wir können auch *Named Parameters* für nur einzelne Werte unserer Wahl nehmen:

```r
hello("hi", name="marc")
```

```
[1] "hi marc"
```

Da wir bei unserer `hello` Funktion zwei Parameter mit Defaults haben, 
können wir auch *Named Parameters* verwenden,
um nur einen der beiden Werten zu überschreiben:

```r
hello(name="marc")
```

```
[1] "hi marc"
```

Wenn wir das `name=` vergessen würde, kannst dir ja vorstellen was dann passieren würde?

```r
hello("marc")
```

```
[1] "marc stranger"
```
