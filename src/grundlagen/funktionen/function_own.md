# Eigene Funktionen schreiben

Lass uns eine Funktion schreiben, die `Hello World` in die Konsole schreibt:

```r
hello <- function() {
  print("Hello World")
}
```

Mit dem Keyword `function` gefolgt von `()` Klammern erstellen wir eine komplett neue, eigene Funktion.
In den `{}` Klammern, den Funktionskörper, 
müssen wir dann nur noch Code schreiben, 
der ausgeführt werden soll,
jedes Mal wenn die Funktion aufgerufen wird.

Wie dir wahrscheinlich schon aufgefallen ist, 
wird die Funktion in einer Variable abgespeichert.
Um sie aufzurufen, benutzen wird den Namen dieser Variable gefolgt von `()` Klammern.

```r
hello()
```

```
[1] "Hello World"
```

## Parameter

Wenn wir jetzt eine `hello` Funktion schreiben wollen, 
die für viele verschiedene Personen `"Hello <Irgendein Name>"` in die Konsole ausgibt, dann könnten wir
für alle verschiedenen Fälle eine komplett neue Funktion schreiben, wie z.B. `hello_marc` und `hello_sven` usw.
oder wir könnten unserer bestehende `hello` Funktion erweitern, 
indem wir bei jedem Aufruf auch einen Namen mitgeben.

```r
hello <- function(name) {
  print(paste("Hello", name))
}
```

Innerhalb der `()` Klammern einer Funktion kommen die sogenannten Funktionsparameter.
Wir könnten beliebig viele Parameter für unsere Funktion definieren.
Innerhalb unseres Funktionskörpers, die `{}` Klammern, 
können wir jetzt den Parameter `name` wie als wäre es eine Variable verwenden,
aber wir müssen jetzt immer, wenn wir die Funktion `hello` benutzen wollen,
innerhalb der `()` Klammern auch einen Namen mitgeben:

```r
hello("Marc")
```

```
[1] "Hello Marc"
```

Wenn wir das nicht machen, dann bekommen wir einen Error, 
da unser Code erwartet dass `name` existiert:

```r
hello()
```

```
Error in hello() : argument "name" is missing, with no default
```

Die Errormessage sagt schon `with no default`. 
Wenn wir einen Defaultwert für den Paramter `name` definieren,
dann müssten wir nicht unbedingt jedes Mal einen Namen in den Funktionsaufruf mit reinschreiben.

```r
hello <- function(name = "stranger") {
  print(paste("Hello", name))
}
```

Jetzt würde unsere Funktion auch mit nur `hello()` funktionieren:

```r
hello()
```

```
[1] "Hello stranger"
```

## Neue Variablen innerhalb einer Funktion

Nicht nur die Funktionsparameter innerhalb der `()` Klammern, 
sondern auch neue Variablen, die wir innerhalb des Funktionsköprers definieren, 
existieren nur dort.
Wenn wir versuchen sollten außerhalb der Funktion auf diese Variablen zuzugreifen,
dann bekommen wir einen Error.

```r
hello <- function(name = "stranger") {
    ## Erstelle eine neue Variable innerhalb der "hello" Funktion
    greeting <- paste("Hello", name)
    print(greeting)
}

greeting  ## Existiert außerhalb der Funktion nicht
```

```
Error: object 'greeting' not found
```
