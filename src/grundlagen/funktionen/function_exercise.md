# Übungsaufgaben

1. Schreibe eine Funktion, die zwei Zahlen addiert und das Ergebnis zurückgibt.

2. Erstelle eine Funktion, die das Minimum und das Maximum aus einer Liste von Zahlen zurückgibt.

3. Schreibe eine Funktion, die eine Zahlenfolge nimmt und die geraden Zahlen zurückgibt.

4. Erstelle eine Funktion, die einen String und eine Zahl nimmt und den String so 
oft wiederholt wie die Zahl angibt.

5. Schreibe eine Funktion, die eine Celsius-Temperatur in Fahrenheit umwandelt.
Die Formel um Celcius zu Fahrenheit umzuwandeln lautet: `°F = (°C x 1.8) + 32`
