# return

Funktionen können auch auch etwas zurückgeben, aber was bedeutet das überhaupt?

Stell dir eine Funktion als ein Helfer vor. 
Wenn Du eine Funktion aufrufst, dann schickst Du den Helfer los, um eine Aufgabe zu erledigen.
Bis jetzt haben wir den Helfer immer nur los geschickt und gegebenfalls in die Konsole schreiben lassen.
Nun kann man aber jetzt den Helfer los schicken und auf ihn warten bis er etwas zurück bringt.

Für das "Zurückbringen" brauchen wir das `return()` Statement.

```r
do_stuff <- function() {
    do_something()
    return(some_value)
}
```

Vorher sahen unserer Funktionen in etwa so aus:

```r
do_stuff <- function() {
    do_something()
    print_something_to_console()
}
```

Zum Beispiel hier eine `add` Funktion, die zwei Werte nimmt, miteinander addiert und
in die Konsole ausgibt:

```r
add <- function(a, b) {
    print(paste("a + b =", a + b))
}

add(5, 3)
```

```
[1] "a + b = 8"
```

Aber wenn wir mit dem Ergebnis von `add(5, 3)` weiter rechnen wollen, 
dann funktioniert dies nicht, weil unsere `add` Funktion, 
nicht das Ergebnis zurückgibt, sondern etwas in die Konsole ausgibt.

Mit `return(a + b)` innerhalb des Funktionskörpers können wir das Ergebnis von `a + b`,
wo auch immer die Funktion aufgerufen wird, zurückgeben, um damit weiterarbeiten zu können.

Lass zum Beispiel das Ergebnis von `add` mal 10 nehmen:

```r
add <- function(a, b) {
    return(a + b)
}

x <- add(5, 3) * 10

print(x)
```

```
[1] 80
```

Unsere `add` Funktion tut jetzt das Ergebnis von `a + b` zurückgeben,
dass bedeutet, dass das `add(5, 3)` von der Zeile, wo wir `x` deklarieren,
evaluiert und mit dem Ergebnis ausgetauscht wird.
Sprich R macht aus `x <- add(5, 3) * 10` => `x <- 8 * 10`.

## Automatisches "returnen"

R tut automatisch den letzten Wert einer Funktion oder eines `{}` Blockes "returnen".

Dies beudetet, dass dies hier:

```r
add <- function(x, y) {
    return(x + y)
}
```

dass selbe tut, wie dieses hier:

```r
add <- function(x, y) {
    x + y
}
```

Dies nennt man auch "implicit return" in Programmierfachchinesisch, weil es implizit und
nicht explizit ist.

Apropos auch ganz verrückt, wenn wir eine Funktion haben, 
die sich in eine Zeile schreiben lässt, 
dann können wir sie auch so schreiben:

```r
add <- function(x, y) x + y
```

## return als Exitpoint

Warum gibt es dann `return` wenn R automatisch die letzte Zeile einer Funktion "returnt"?

`return` dient auch als Exitpoint einer Funktion. 
Sobald unsere Funktion eine Zeile erreicht hat die "return" in sich hat, 
werden alle folgenden Zeilen danach nicht mehr ausgeführt:

```r
can_drive <- function(age) {
    if (age < 18) {
        return(FALSE)
    } else {
        return(TRUE)
    }
    print("This line will never be executed!")
}

can_drive(15)
can_drive(29)
```

```
[1] FALSE
[1] TRUE
```

`return` richtig zu verwenden, kann Deine Skills als Programmieranfänger verzehnfachen.
Also wenn Du es nicht auf Anhieb alles verstanden hast,
sei nicht demotiviert. 
Manche Dinge brauchen einfach länger zum Verstehen.
Les Dir dieses Kapitel nochmal in Ruhe durch oder schau Youtube Videos,
die genau dieses Thema versuchen zu erklären.
Vetrau mir es wird sich für Dich lohnen.
