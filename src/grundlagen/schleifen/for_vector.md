# mit Zahlen-Vektoren

Im vorherigen Kapitel haben wir schon Vektoren als Sequenz verwendet,
denn bedenke `1:10` ist nichts anders als `c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)`.
Aber wir können auch natürlich ohne Probleme jeden beliebigen Vektor benutzen
um über diesen zu iterieren.

Zum Beispiel können wir über einen Zahlenvektor mit verschieden Zahlen gehen und 
jede Zahl addieren 
und das Ergebnis in einer neuen `summe` Variable speichern.

```r
## Der Zahlenvektor
numbers <- c(1, 43, 56, 134, 1230, 34, 5, 20)
## Das Ergebnis der Addition. Beginnt bei null.
summe <- 0;

## Gehe über numbers Vektor
for (number in numbers) {
    ## Überschreibe `summe` mit dem alten Wert von `summe` plus `number`
    summe = number + summe
}

## Gebe das Ergebnis in der Konsole aus
print(summe)
```

```
[1] 1523
```

Das selbe macht auch die `sum` Funktion, also können wir ganz einfach unser Code testen:

```r
sum(numbers)
```

```
[1] 1523
```

# mit String-Vektoren

Vektoren können nicht nur aus Zahlen bestehen. 
Es gibt auch Vektoren die aus Strings oder Booleans bestehen.
Egal aus welchen Datentyp der Vektor auch besteht, 
wir können alle Arten von Vektoren mit einer `for` Schleife verwenden.
Wir müssen nur sicher gehen, dass unser Code keinen Error wirft,
denn bedenke man kann nicht plus, minus oder geteilt mit Strings nehmen.

```r
## character/string Vektor
animals <- c("koala", "cat", "dog", "panda")

for (animal in animals) {
    print(animal) ## Gebe das Tier in die Konsole aus
}
```

```
[1] "koala"
[1] "cat"
[1] "dog"
[1] "panda"
```
