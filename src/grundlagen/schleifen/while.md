# while

Die `while` Schleife wird verwendet, 
um einen Codeblock so lange auszuführen, wie eine bestimmte Bedingung erfüllt ist. 
Hier ist die grundlegende Syntax:

```r
while (Bedingung) {
  ## Code
}
```

Die Bedingung ist eine logische Aussage (z.B. `x < 10`), 
die entweder `TRUE` oder `FALSE` ist. 
Solange die Bedingung `TRUE` ist, 
wird der Codeblock in den geschweiften Klammern {} ausgeführt.

```r
x <- 1

while (x <= 5) {
  ## Gebe x in der Konsole aus
  print(x)
  ## Erhöhe x um 1
  x <- x + 1
}
```

```
[1] 1
[1] 2
[1] 3
[1] 4
[1] 5
```

In diesem Beispiel startet `x` mit dem Wert 1. 
Die Bedingung in der `while` Schleife lautet `x <= 5`, 
was bedeutet "solange x kleiner oder gleich 5 ist". 
Innerhalb der Schleife wird zuerst der aktuelle Wert von x mit der `print` Funktion ausgegeben, 
dann wird x um eins erhöht. 
Die Schleife wird solange wiederholt, bis x größer als 5 ist.

Wichtig ist, dass wir innerhalb der `{}` Klammern x um eins erhöhen,
denn sonst würde unsere Bedingung `x <= 5` niemals `FALSE` werden und
unsere `while` Schleifen würde niemals enden.

Niemals endende Schleifen nennt man auch Endlos-Schleifen.

## Endlos-Schleifen

Endlos-Schleifen hören sich erstmal kontraproduktiv an.
Warum sollte ich ein Stück Code schreiben, welches niemals endet?
Wenn man eine Videospiel programmiert, macht es durchaus Sinn eine
Endlos-Schleife zu erstellen, denn man muss 60 oder 100erte Male
pro Sekunde das Spielbild updaten und neurendern.

Aber auch in anderen Szenarien kann es durchaus Sinn machen eine Endlos-Schleife
zu benutzen, um dann gegebenfalls mit einem `break` Statement diese zu abzubrechen,
falls eine Bedingung eingetreten ist, wie zum Beispiel auf eine Antwort
vom User warten, die jederzeit kommen könnte.

Diese Schleife hier läuft solange bis counter den Wert 20.000 erreicht hat:

```r
counter <- 1

while (TRUE) {
    print(paste("Iteration", counter))
    counter <- counter + 1
    if (counter > 20000) break
}
``` 

Die Bedingung dieser `while` Schleife ist `TRUE` und naja ... diese Aussage ist immer wahr,
aber mit `if (counter > 20000) break` gehen wir sicher, dass unser PC nicht überhitzt
und unser Programm auch mal beendet wird.

### Übungsaufgaben

1. Schreibe eine while-Schleife, die Elemente aus einem Vektor entfernt, bis dieser leer ist.

2. Mit `rnorm` bekommt man eine zufällige Zahl. 
Gebe solange die Zufallszahlen in einer Konsole aus, bis eine Zahl größer als 1 kommt.

3. Nutze die Schleife von der Aufgabe 2 und füge `next` hinzu, um alle Minuszahlen zu überspringen.

4. Lass den User eine Zahl zwischen 1 und 10 erraten. 
Nachdem 5. Fehlversuch ist das Spiel vorbei.

5. Schreibe ein Multiplikationsspiel mit Zufallsaufgaben und 
sobald der User das Ergebnis falsch eingibt, ist das Spiel vorbei.
