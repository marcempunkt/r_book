# mit Listen

Über Listen kann man auch Schleifen, aber hier müssen wir vorsichtig sein!
Während bei einem Vektor nur ein Datentyp erlaubt ist, ist in einer Liste alles erlaubt.
Zahlen, Vektoren, Listen, Booleans, alles kann hier auftauchen.
Über Listen zu Schleifen kann also zu sehr unstabilen Code führen, 
der schnell crashen kann, wenn wir nicht 100%ig sicher sein können, dass
unser Code auch für den jeweiligen Datentyp funktioniert.

```r
my_list <- list(c(5, 2, 4), 
                "cat", 
                "dog", 
                TRUE, 
                3, 
                c("koala", "panda", "rabbit"))

for (item in my_list) {
    print(item)
}
```

```
[1] 5 2 4
[1] "cat"
[1] "dog"
[1] TRUE
[1] 3
[1] "koala"  "panda"  "rabbit"
```

Um sicher zu gehen mit welchen Datentyp man es zu tun hat, 
könnte man die `is.` Funktionen benutzen.
Für so ziemlich alle verschiedenen Datentypen gibt es eine `is` Funktion,
die uns einen Boolean zurück gibt.
Bei Zahlen würde man `is.numeric` benutzen oder bei Strings `is.character`.

```R
my_list <- list(c(5, 2, 4), 
                "cat", 
                "dog", 
                TRUE, 
                3, 
                c("koala", "panda", "rabbit"))

for (item in my_list) {
    if (is.numeric(item)) {
        print("is number")
    } else if (is.character(item)) {
        print("is string")
    } else if (is.logical(item)) {
        print("is boolean")
    } else {
        print("i dont know which type it is...")
    } 
}
```

```R
[1] "is number"
[1] "is string"
[1] "is string"
[1] "is boolean"
[1] "is number"
[1] "is string"
```

Oder man könnte auch die `typeof` Funktion, die uns einen String mit den Namen von dem Datentyp zurückgibt.

```R
my_list <- list(c(5, 2, 4), 
                "cat", 
                "dog", 
                TRUE, 
                3, 
                c("koala", "panda", "rabbit"))

for (item in my_list) {
    ## speicher typeof in einer Variable, 
    ## damit wir es nicht wieder und wieder machen müssen
    type <- typeof(item)
    
    if (type == "integer" || type == "double") {
        print("is number")
    } else if (type == "character") {
        print("is string")
    } else if (type == "logical") {
        print("is boolean")
    } else {
        print(paste("i dont know which type it is:", type))
    }
}
```

```R
[1] "is number"
[1] "is string"
[1] "is string"
[1] "is boolean"
[1] "is number"
[1] "is string"
```

Vielleicht ist Dir diese Zeile im Codebeispiel aufgefallen 
`if (type == "integer" || type == "double")`.

In vielen Programmiersprachen zieht man einen Unterschied zwischen
Ganzahlen, den sogenannten `Integer`, und den Fließkommazahlen, den `Double`.

Beides sind ganz normale Zahlen wie im [Kapitel Number](../daten_typen/number.md)
schon besprochen, aber der Computer muss technisch einen Unterschied zwischen beider
dieser Zahlen nehmen, denn eine Fließkommazahl mit den Nachkommastehlen
braucht mehr Platz zum Speichern als eine normale, ganze Zahl.

R tut automatisch jede Zahl, die wir eintippen als `double` abspeichern,
damit wir nicht nachdenken müssen wenn wir solch eine Zeile Code schreiben:

```r
1 / 0.3
```
