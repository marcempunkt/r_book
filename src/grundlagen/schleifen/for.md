# for

Die `for` Schleife gibt es so ziemlich in jeder Programmiersprache.
Kurzgesagt funktioniert sie so: __Für die Variable `i` in einer Sequenz führe Code aus.__
In R-Code sieht es ungefähr so aus:

```r
for (i in sequence) {
  ## ...code
}
```

Um die `for` Schleife besser verstehen zu können, 
müssen wir uns ein Beispiel anschauen:

```r
for (i in 1:10) {
  print(i)
}
```

```
[1] 1
[1] 2
[1] 3
[1] 4
[1] 5
[1] 6
[1] 7
[1] 8
[1] 9
[1] 10
```

`i` ist die Variable, die jeden Wert innerhalb einer Sequenz animmt.
In diesem Beispiel ist `i` beim ersten mal durchlaufen der Schleife `1`
und dann beim zweiten Mal `2` und so weiter bis `10`. 
`i` nimmt dabei pro Durchlauf jeden einzelnen Wert der Sequenz `1:10` an.

<div class="tippbox">
Die Sequenz <b>1:10</b> ist nichts anderes als ein Vektor der alle Zahlen von 
1 bis 10 hat: <b>c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)</b>.
</div>


Wir sind nicht gezwungen unsere Schleifen-Variable `i` zu nennen.
Wir können unsere Schleifenvariable so benennen,
wie es gerade am besten zu unserem Code passt.

Für jeden Durchlauf wird der Code innerhalb der `{}` Klammern ausgeführt,
deswegen sehen wir in der Konsole 1 bis 10 je in einer Zeile.
Der Code innerhalb der `{}` kann so lang sein, wie wir wollen. 
Lass uns zum Beispiel eine `for` Schleife schreiben, 
die von 1-12, für die Monate in einem Jahr, geht und
in die Konsole schreibt, ob es ein Sommer oder Winter Monat ist:

```r
for (month in 1:12) {
    if (month >= 6) {
        print(paste("Summer, month", month))
    } else {
        print(paste("Winter, month", month))
    }
}
```

```
[1] "Winter, month 1"
[1] "Winter, month 2"
[1] "Winter, month 3"
[1] "Winter, month 4"
[1] "Winter, month 5"
[1] "Summer, month 6"
[1] "Summer, month 7"
[1] "Summer, month 8"
[1] "Summer, month 9"
[1] "Summer, month 10"
[1] "Summer, month 11"
[1] "Summer, month 12"
```

## next und break

Innerhalb einer Schleife können wir auch `next` und `break` benutzen.

`next` tut jeden weiteren Code, der kommen würde, überspringen und
geht direkt in die nächste Schleifeniteration.

```r
## Schleife von 1 bis 10
for (i in 1:10) {
  ## Wenn es durch 3 teilbar ist, dann überspringe
  if (i %% 3 == 0) {
    next
  }
  ## Sonst printe die Zahl in der Konsole
  print(i)
}
```

```
[1] 1
[1] 2
[1] 4
[1] 5
[1] 7
[1] 8
[1] 10
```

`break` bricht eine Schleife sofort ab und es wird keine weiter Iteration und kein
weiterer Code der Schleife ausgeführt.

```r
## Schleife von 1 bis 10
for (i in 1:10) {
  ## Wenn i gleich 5 ist, dann breche die Schleife ab
  if (i == 5) {
    break
  }
  ## Sonst printe die Zahl in der Konsole
  print(i)
}
```

```
[1] 1
[1] 2
[1] 3
[1] 4
```
