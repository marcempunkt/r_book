# Übungsaufgaben

1. Addiere alle ungeraden Zahlen zwischen 1 und 100 miteinander

2. Zähle wie oft ein Buchstabe in einem String vorkommmt

3. Lass den User eine Zahl eingeben und berechne den faktorialen Wert

4. Lass den User ein Text eingeben und zähle wie viele Konsanten und Vokale darin vorkommmen

5. Berechne Medium, Maximum und Minimum von einem Zahlenvektor

6. Lass den User eine Zahl eingeben und addiere alle Zahlen bis null darauf.
Sprich wenn der User die Zahl 89 eingibt, dann addiere 88, 87, 86 ... bis 0 auf
auf die ursprüngliche Zahl.

7. Tannenbaum
