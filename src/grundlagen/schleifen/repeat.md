# repeat

Anstelle `while (TRUE) { ... }` können wir auch `repeat` benutzen.

Eine `repeat` Schleife ist dabei immer endlos und wir müssen sicher stellen,
dass wir ein `break` Statement haben.

```r
counter <- 1

repeat {
    print(paste("Iteration", counter))
    counter <- counter + 1

    if (counter > 20000) break
}
```

Ob wir jetzt `while (TRUE)` oder `repeat` benutzen,
macht am Ende des Tages keinen großen Unterschied.
Ich bevorzuge `repeat`, weil man sich, 
im Falle einer Endlosschleife, ein paar Schriftzeichen sparen kann,
aber du kannst es so machen wie du es willst!
