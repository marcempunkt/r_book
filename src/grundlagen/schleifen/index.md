# Schleifen

In so ziemlich jeder Programmiersprache gibt es Schleifen oder auch "Loops" auf Englisch genannt.

Schleifen ermöglichen es Dir, bestimmte Aktionen mehrmals wiederholen zu lassen
(in Programmierfachchinesisch wird ein solche Wiederholung 
einer Schleife auch "Iteration" genannt). 
Dies ist besonders nützlich, wenn du Code hast, 
der viele Male hintereinander durchgeführt werden muss und
Du nicht 100-mal hintereinander den selben Code copy-pasten willst.

In R gibt es drei verschiedene Arten von Schleifen,
die für verschiedene Usecases verwendet werden:

- [for](for.md) 
- [while](while.md) 
- [repeat](repeat.md) 
