# mit Matrizen

Wir können über eine Matrix genau so schleifen wie wir es beim Vektor getan haben.

```r
## Erstelle eine 3x3 Matrix mit den Werten von 1 bis 9
my_matrix <- matrix(1:9, nrow=3, ncol=3)
```

```
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

```R
for (i in my_matrix) {
  print(i)
}
```

```
[1] 1
[1] 2
[1] 3
[1] 4
[1] 5
[1] 6
[1] 7
[1] 8
[1] 9
```

Obwohl eine Matrix zwei-dimensional ist, kann man es auch als ein Vektor repräsentieren und
das ist auch was im Hintergrund passiert, weswegen wir syntaktisch auf nichts achten müssen.

Wie man auch an dem Output der Schleife erkennen kann, wenn man über eine Matrix schleift,
dann geht man von Spalte zu Spalte bis man durch die ganze Matrix durchgeschleift ist.

Wir können aber auch Zeile für Zeile und 
jeweils bei jeder Zeile über alle Spalte gehen.
Dies ist ein bisschen komplizierter, denn hierfür müssen wir zwei `for` Schleifen
ineinander schreiben:

```r
## Eine 3x3 Matrix
my_matrix <- matrix(1:9, nrow=3, ncol=3)

## Die erste `for` Schleife geht über die Zeilen
for (row in 1:nrow(my_matrix)) {
  ## Die zweite `for` Schleife geht für die jeweilige Zeile über alle Spalten
  for (col in 1:ncol(my_matrix)) {
      ## Gebe in alle wichtigen Information in der Konsole aus
      print(paste('Row', row, 'col', col, 'value', my_matrix[row, col]))
  }
}
```

```
[1] "Row 1 col 1 value 1"
[1] "Row 1 col 2 value 4"
[1] "Row 1 col 3 value 7"
[1] "Row 2 col 1 value 2"
[1] "Row 2 col 2 value 5"
[1] "Row 2 col 3 value 8"
[1] "Row 3 col 1 value 3"
[1] "Row 3 col 2 value 6"
[1] "Row 3 col 3 value 9"
```

In der ersten Schleife `for (row in 1:nrow(my_matrix)) { ... }` gehen wir 
über alle Zeilen der Matrix. `nrow(my_matrix)` gibt die Anzahl der Zeilen zurück,
also in diesem Fall 3. 
Der Ausdruck `1:nrow(my_matrix)` erzeugt eine Sequenz von 1 bis 3.

Die zweite Schleife `for (col in 1:ncol(my_matrix)) { ... }` geht über alle Spalten der Matrix.
Ähnlich wie zuvor gibt `ncol(my_matrix)` die Anzahl der Spalten zurück
und `1:ncol(my_matrix)` erzeugt wieder eine Sequenz von 1 bis 3.

`print(paste('Row', row, 'col', col, 'value', my_matrix[row, col]))` wird für jede Zelle 
in der Matrix ausgeführt. 
`paste` ist eine Funktion, die mehrere Zeichenketten zu einer einzigen Zeichenkette verbindet. 
In diesem Fall werden die Zeichenketten 'Row', der Wert von `row`, 'col', 
der Wert von `col` und 'value' und der Wert in der Matrixzelle 
`my_matrix[row, col]` als String zusammengefügt und dann mit print ausgegeben.
