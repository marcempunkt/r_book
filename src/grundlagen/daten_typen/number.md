# Number

Einer der wohl wichtigsten und offensichtlichsten Datentypen einer Programmiersprache 
ist die Nummer oder Zahl.

In R heißen Nummern und Zahlen `Number`.
Im vorherigen Kapitel haben wir die Variable `my_age` erstellt, 
die die "Number" `25` zugewiesen bekommen hat.

Wie in der Mathematik können wir mit `Number`s auch rechnen:

#### Plus +

```r
x <- 5
y <- 3.5
z <- x + y
z ## Console: [1] 8.5
```

#### Minus -

```r
x <- 5
y <- 3.5
z <- x - y
z ## Console: [1] 1.5
```

#### Mal *

```r
x <- 5
y <- 3.5
z <- x * y
z ## Console: [1] 17.5
```

#### Geteilt /

```r
x <- 5
y <- 3.5
z <- x / y
z ## Console: [1] 1.428571
```

#### Hoch ^

```r
x <- 5
y <- 3.5
z <- x ^ y
z ## Console: [1] 279.5085
```

#### Modulus %%

Der Modulus `%%` wird verwendet, um den Rest einer Division zu berechnen.

Kannst du dich vielleicht noch erinnern, 
wie du in der Grundschule geteilt rechnen gelernt hast und 
am Ende der Rechnung den Rest angegeben hast, 
z.b. `5 / 2 = 2 Rest 1`.

Der Modulus funktioniert genau so, nur dass wir nur den Rest bekommen! 
Also `5 %% 2` gibt uns die Zahl `1` oder `11 %% 3` ergibt `2`.

Zum Beispiel können wir den Modulus verwenden, 
um zu erkennen ob eine Zahl gerade oder ungerade ist,
denn wenn `x %% 2` null ergibt, dann ist die Zahl durch zwei teilbar 
und wenn es eins ergibt, dann eben nicht.

```r
5 %% 2 ## Console: [1] 1  Die Zahl ist ungerade
8 %% 2 ## Console: [1] 0  Die Zahl ist gerade
```

## Übungsaufgaben

Hier ein paar Übungsaufgaben, die du nun lösen kannst, 
um deine Programmierskills auf die Probe zustellen!

1. Berechne die Tage anhand des Alters einer Person.

2. Berechne wie viele Sekunden x Stunden hat, wo x eine beliebige Zahl ist.

3. Ermittle anhand des Geburtsjahres wie alt eine Person ist

4. Ermittle den Rest einer Division von zwei Zahlen

5. Berechne den Flächeninhalt eines Dreiecks (a: 5cm, b: 5cm, c: 10cm)
