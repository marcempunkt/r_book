# Matrix erweitern

Mit `rbind` und `cbind` können wir eine schon erstellte Matrix erweitern.

Um eine neue Zeile (row) einer Matrix hinzuzufügen, müssen wir die `rbind` Funktion benutzen.
`rbind` steht in diesem Fall "binde" (also füge hinzu) eine neue "row".

```r
x <- matrix(data = c("one", "two", "three", "four"), 
            nrow = 2, 
            ncol = 2)
```

```
     [,1]  [,2]
[1,] "one" "three"
[2,] "two" "four" 
```

```R
x <- rbind(x, c("five", "six"))
```

```
     [,1]   [,2]
[1,] "one"  "three"
[2,] "two"  "four" 
[3,] "five" "six"
```

das selbe mit `cbind` aber um eine Spalte (column) hinzufügen:

```r
x <- matrix(data = c("one", "two", "three", "four"), 
            nrow = 2, 
            ncol = 2)
```

```
     [,1]  [,2]
[1,] "one" "three"
[2,] "two" "four" 
```

```R
x <- cbind(x, c("five", "six"))
```

```
     [,1]  [,2]    [,3]
[1,] "one" "three" "five"
[2,] "two" "four"  "six"
```

`rbind` und `cbind` akzeptieren eine beliebig große Anzahl an Vektoren 
oder Matrixen, um daraus eine neue Matrix zu erstellen, 
deswegen vergesse nicht deine Matrix-Variable, in diesem Fall `x`,
zu überschreiben.
