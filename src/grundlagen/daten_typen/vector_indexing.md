# Indexing

Wenn wir jetzt wissen wollen was an einer bestimmten Stelle in unserer Einkaufsliste steht, 
brauchen wir die `[]` Klammern. 

Innerhalb der `[]` Klammern kommt dann die Zahl für welchen Index wir den Wert haben wollen.
Für die erste Stelle ist der Index 1 und für die zweite 2 und so weiter.

```r
shopping_list <- c("Ketchup", "Tomaten", "Paprika", "Banane")
shopping_list[1]  # Console: [1] "Ketchup"
shopping_list[3]  # Console: [1] "Paprika"
```

Wenn wir die Werte von 1 bis 3 haben wollen, 
dann können wir innerhalb der `[]` Klammern auch einen Vektoren mit den Indexen 
(oder auch Indizen in richtigeren Deutsch genannt), die wir haben wollen reinschreiben.

```r
shopping_list[c(1, 2, 3)]  # Console: [1] "Ketchup" "Tomaten" "Paprika"
shopping_list[c(1, 3)]     # Console: [1] "Ketchup" "Paprika"
```

Wie du sehen kannst können wir auch nur den 1. und 3. Index bekommen, wenn wir die 2 weglassen.
Wir hätten aber auch `c(1, 2, 3)` abkürzen können. 
R gibt uns für sogenannte `ranges` eine bequeme Syntax.

```r
shopping_list[1:3]  # Console: [1] "Ketchup" "Tomaten" "Paprika"
```

Die Syntax `1:3` erstellt einen Vektor mit den Zahlen `1, 2, 3`. 
Wie groß die Range sein soll, können wir selber bestimmen, 
so würde zum Beispiel `1:100` einen Vektor von 1 bis 100 erstellen. 
Du könntest auch den Vektor selber per Hand erstellen: `c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ..., 100)` 
oder du wenn du kein Masochist bist, dann kannst du auch einfach nur `1:100` eintippen.

Wir sind aber nicht beschränkt auf positive Zahlen eine negative Range ist auch möglich.
So würde `-100:100` einen Vektor mit den Zahlen zwischen -100 bis +100 erstellen.

Was passiert jetzt, wenn wir eine zu große Range bekommen wollen als `shopping_list` hat?

```r
shopping_list[1:5]  # Console: [1] "Ketchup" "Tomaten" "Paprika" "Banane"  NA
```

Wie wir sehen können, wenn wir einen Wert von einem Index uns holen, 
der noch nicht existiert, bekommen wir `NA`, "Not Available", zurück.

## Indexe ausschließen

Wir können angeben welchen Index oder welche Indexe wir haben wollen, aber wir können auch das
Gegenteil und zwar angeben welche Indexe wir nicht haben wollen, indem wir innerhalb der `[]`
Klammern eine Minuszahl eingeben.

```r
shopping_list[-1]  # Console: [1] "Tomaten" "Paprika" "Banane"
```

Um mehrere Indexe auszuschließen, dann müssen wir sicher gehen, 
dass der Vektor oder die Range, die wir innerhalb der `[]` Klammern schreiben, nur aus Minus zahlen besteht.
Denn bedenke eine Range `-1:2` gibt uns einen Vektor `-1  0  1  2`.

Als nächses zeige ich dir verschiedenen Methoden, wie man ein negative Ranges erstellen kann. 
Du musst dir nicht alle merken, außer natürlich mindestens eine.

```r
shopping_list[-1:-2]     # Console: [1] "Paprika" "Banane"
shopping_list[-c(1, 2)]  # Console: [1] "Paprika" "Banane"
shopping_list[-(2:4)]    # Console: [1] "Ketchup"
shopping_list[-c(2:4)]   # Console: [1] "Ketchup"
```

## Ein Vector erweiteren

Wir können auch einen Vektor erweitern, 
indem wir an der nächsten freie Stelle nach und nach Werte zuweißen:

```r
shopping_list[5] <- "Orangensaft"
shopping_list  # Console: [1] "Ketchup" "Tomaten" "Paprika" "Banane" Orangensaft"
```

Wenn wir einen Index wieder entfernen wollen, dann müssen wir einen kleinen Trick anwenden.
Mit einer Minuszahl als Index bekommen wir alles bis auf den Index. Was wir bekommen ist ein
komplett neuer Vektor, nur halt ohne den Index den wir angegeben haben als Minuszahl, mach Sinn oder? 
Diesen Vektor können wir dann benutzen, um den originalen Wert von `shopping_list` zu überschreiben:

```r
# Gibt uns einen neuen Vektor mit: [1] "Ketchup" "Tomaten" "Paprika" "Banane"
shopping_list[-5]

# Aber noch ist shopping_list: [1] "Ketchup" "Tomaten" "Paprika" "Banane" Orangensaft"
shopping_list

# Jetzt überschreiben wir shopping_list mit den neuen Vektor
shopping_list <- shopping_list[-5]

# Et voilá
shopping_list # Console: [1] "Ketchup" "Tomaten" "Paprika" "Banane"
```
