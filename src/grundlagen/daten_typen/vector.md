# Vector

Vektoren (oder "Vector" auf Englisch) sind Listen, die aus mehreren Einträge bestehen.

Stell dir einen Vektor wie eine Einkaufsliste vor. 
In dieser Liste können viele verschiedene Sachen drinne stehen, 
wie z.B. `"Ketchup" "Tomaten" "Paprika" etc...`. 

Um einen Vector in R zu erstellen benutzen wir den `c` Befehl.
`c` steht in diesem Fall für combine, weil mehrere Items zu einer Liste (einem Vektor) kombiniert werden.
Innerhalb der `()` Klammer kommen mit einem Komma getrennt alle Werte rein, 
die wir in einem Vektor vereinen wollen.

```r
vec <- c("Ketchup", "Tomaten", "Paprika", "Banane")
vec  ## Console: [1] "Ketchup" "Tomaten" "Paprika" "Banane"
```

Wichtig zu wissen ist es, dass innerhalb eines Vektors nur ein Datentyp erlaubt ist.
Hier zum ein Beispiel ein Vektor, der nur aus Zahlen besteht:

```r
vec <- c(23, 63, 39, 35, 84, 58)
vec  ## Console: [1] 23 63 39 35 84 58
```

Wir können aber nicht gleichzeitig Zahlen und Text in einen Vektor speichern, 
beziehungsweise können wir das doch, 
aber R tut automatisch alle `number`s zu `character`s konvertieren, 
damit unser Code ohne Error ausgeführt werden kann.

```r
vec <- c(1, 2, 4, "A", 5, "B")
vec  ## Console: [1] "1" "2" "4" "A" "5" "B"
```

Wie wir sehen können, hat R automatisch für uns die Zahlen 
innerhalb der Klammern von `c` zu `character` konvertiert.

Man kann auch mit `c` mehrer Vectoren miteinander kombinieren.

```r
vec <- c(
    c(1, 2, 3),
    c(4, 5, 6)
)
vec  ## Console: [1] 1, 2, 3, 4, 5, 6
```
