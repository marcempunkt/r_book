# switch

`switch` funktioniert wie ein Entscheidungsbaum, 
der eine bestimmte Aktion aus einer Liste von Optionen ausführt, 
basierend auf dem Wert einer Variable oder eines Ausdrucks.

Stell dir vor, du hast eine Box mit verschiedenen Früchten und du möchtest wissen, 
welche Farbe eine bestimmte Frucht hat. Zum Beispiel du listest die verschiedenen 
Früchte auf und ordnest ihnen ihre jeweiligen Farben zu, sprich
Apfel = "Rot", Banane = "Gelb", Orange = "Orange" und so weiter.

```r
fruit = "banana"

switch(fruit,
       "apple" = print("red"),
       "banana" = print("yellow"),
       "orange" = print("orange"),
       "pear" = print("green"))
```

In diesem Beispiel, wird je nachdem welchen Wert `fruit` hat, der jeweilige `print` Befehl ausgeführt.
An erster Stelle der `()` Klammern des `switch` Befehls kommt die Variable, die wir "matchen" wollen,
gefolgt von allen Möglichkeiten, die wir abdecken wollen, getrennt mit einem Komma.
Bei jeder Möglichkeit (auch "case" genannt) kommt nach dem `=` Zeichen weiterer R-Code, der ausgeführt
werden soll.

Wenn `fruit` in dem Fall einen anderen Wert hat als vom `switch` abgedeckt wird, dann wird kein Code
ausgeführt. Natürlich können wir auch einen "default case" innerhalb der `switch` definieren.

```r
grade = "A"

switch(grade,
       "A" = print("Great"),
       "B" = print("Good"),
       "C" = print("Ok"),
       "D" = print("Bad"),
       "F" = print("Terrible"),
       print("No Such Grade"))
```

Hier tuen wir je nach Note (A-F) den jeweiligen Begriff in die Konsole "printen".
Wenn wir aber jetzt `grade` den Wert `Z` geben, wird "No Such Grade" in der
Konsole "geprintet".

## switch mit Zahlen

Anstelle von Text lass uns versuchen ein `switch` Statement mit Zahlen zu schreiben!

```r
grade <- 2
switch(grade,
       6 = print("Did you study at all?"),
       5 = print("You need to study more"),
       4 = print("Well next time will be better"),
       3 = print("Good"),
       2 = print("Great"),
       1 = print("Awesome"),
       print("No such grade"))
```

Ohoh, wenn ich diesen Code ausführe, dann bekomme ich einen Error:

```
Error: unexpected '=' in:
"switch(grade,
       6 ="
> Error: unexpected ',' in "       5 = print("You need to study more"),"
> Error: unexpected ',' in "       4 = print("Well next time will be better"),"
> Error: unexpected ',' in "       3 = print("Good"),"
> Error: unexpected ',' in "       2 = print("Great"),"
> Error: unexpected ',' in "       1 = print("Awesome"),"
> Error: unexpected ')' in "       print("No such grade"))"
```

Wir dürfen die `"` Zeichen bei den Möglichkeiten nicht vergessen:

```r
grade <- 2
switch(grade,
       "6" = print("Did you study at all?"),
       "5" = print("You need to study more"),
       "4" = print("Well next time will be better"),
       "3" = print("Good"),
       "2" = print("Great"),
       "1" = print("Awesome"),
       print("No such grade"))
```

Sieht gut aus? Dann führen wir den Codeabschnitt mal aus:

```
[1] "You need to study more"
```

Wowowow, die Note war 2! Was ist hier passiert?

`switch` mit Zahlen funktioniert ein bisschen anders.
Anstelle die Variable `grade` mit den jeweiligen Zweigen zu vergleichen,
um dann den richtigen Code auszuführen, wird der x'te Zweig ausgeführt.
In diesem Fall ist `grade` 2 also wird der Zweig 
`"5" = print("You need to study more")` ausgeführt.
Die `"5"` wird dabei von R komplett ignoriert. Wir könnten den selben Code auch so schreiben:

```r
grade <- 6
message <- switch(grade,
                  "Did you study at all?",
                  "You need to study more",
                  "Well next time will be better",
                  "Good",
                  "Great",
                  "Awesome")
print(message)
```

Zwei Sachen sind hier zu bemerken: Die `"` und `=` wurde entfernt und gleichzeitig
auch die `print` Befehle, sonst würden wir einen Syntaxerror bekommen. 
Anstelle dass jetzt mit `print` Befehl etwas in der R-Konsole geschrieben wird, 
wird der jeweilige Text zurückgegeben und in der Variable `message` gespeichert,
um es dann in der nächsten Zeile in der Konsole zu "printen".
Wir werden das "zurückgeben" noch im [Kapitel 7.3](grundlagen/funktionen/function_return.md) genauer kennen lernen,
keine Sorge, wenn du es noch nicht so ganz verstanden hast, was hier passiert.

Jetzt müssen wir nur noch die Reihenfolge ändern, damit auch der richtige Zweig
ausgeführt wird:

```r
grade <- 6
message <- switch(grade,
                  "Awesome",
                  "Great",
                  "Good",
                  "Well next time will be better",
                  "You need to study more",
                  "Did you study at all?")
print(message)
```

ODER wir könnten die Variable `grade` mit `as.character()` zu Text konvertieren, 
um den alten obrigen Code zu reparieren:

```r
grade <- 2
switch(as.character(grade),
       "6" = print("Did you study at all?"),
       "5" = print("You need to study more"),
       "4" = print("Well next time will be better"),
       "3" = print("Good"),
       "2" = print("Great"),
       "1" = print("Awesome"),
       print("No such grade"))
```

Ja manchmal können Lösungen auch so einfach sein, aber wir als Programmierer
kommen meist nicht immer sofort auf die "beste" Lösung und deswegen ist irgendeine
Lösung manchmal besser als sich stundenlang den Kopf zu zerbrechen.

## Übungsaufgaben

Hier ein paar Übungsaufgaben mit `switch`:

1. Schreibe ein Programm, dass mit Hilfe eines switch Statements aus einer Note (1-6) die Punktzahl (0-100) zuweist

2. Erkenne anhand einer Zahl zwischen 1-7 um welchen Wochentag es sich handelt

3. Erkenne anhand einer Zahl zwischen 1-12 um welchen Monat es sich handelt
