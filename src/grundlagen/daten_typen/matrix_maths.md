# Mathe mit Matrizen

Mit Matrizen zu rechnen ist nahezu identisch wie mit Vektoren 
im [Kapitel 5.4.1. Mathe mit Vektoren](vector_math.md).

Lass zwei identische 3x3 Matrizen erstellen, um damit zu rechnen.

```r
a <- matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9), 3, 3)
b <- matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9), 3, 3)
```

a:

```
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

b:

```
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

#### Plus +

```r
a + b
```

```
     [,1] [,2] [,3]
[1,]    2    8   14
[2,]    4   10   16
[3,]    6   12   18
```

#### Minus -

```r
a - b
```

```
     [,1] [,2] [,3]
[1,]    0    0    0
[2,]    0    0    0
[3,]    0    0    0
```

#### Geteilt /

```r
a / b
```

```
     [,1] [,2] [,3]
[1,]    1    1    1
[2,]    1    1    1
[3,]    1    1    1
```

#### Mal * 

```r
a * b
```

```
     [,1] [,2] [,3]
[1,]    1   16   49
[2,]    4   25   64
[3,]    9   36   81
```

Um eine echte mathematische Multiplizierung von Matrizen durchzuführen,
benutzen wir `%*%` anstelle `*`:

```r
a %*% b
```

```
     [,1] [,2] [,3]
[1,]   30   66  102
[2,]   36   81  126
[3,]   42   96  150
```

#### Hoch ^

```r
a ^ b
```

```
     [,1]  [,2]      [,3]
[1,]    1   256    823543
[2,]    4  3125  16777216
[3,]   27 46656 387420489
```

#### Modulus %%

```r
a %% b
```

```
     [,1] [,2] [,3]
[1,]    0    0    0
[2,]    0    0    0
[3,]    0    0    0
```

## Rechnen mit Zahlen

Genau so wie bei Vektoren, können wir auch einzelne Zahlen mit Matrizen verrechnen.

```r
a + 10
```

```
     [,1] [,2] [,3]
[1,]   11   14   17
[2,]   12   15   18
[3,]   13   16   19
```

## Rechnen mit Vektoren

Sogar Matrix + Vektor geht ohne Probleme:

```r
a + c(10, 20, 30)
```

```
     [,1] [,2] [,3]
[1,]   11   14   17
[2,]   22   25   28
[3,]   33   36   39
```

## Wichtige Funktionen um mit Matrizen zu rechnen

### diag

### sum

### min

### max

### dim

### t

### as.vector

### rowSums

### colSums

### rowMeans

### colMeans
