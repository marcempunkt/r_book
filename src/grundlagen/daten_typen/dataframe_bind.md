# DataFrame erweitern

Genauso wie wir es in [Kapitel 5.5.1](matrix_bind.md), 
können wir DataFrames mit `rbind` und `cbind` erweitern.

Lass dafür unsere `df` Variable, die wir vorher erstellt haben erweitern:

```
     name age child
1    Marc  25 FALSE
2  Thomas  23  TRUE
3    Leon  26 FALSE
4 Rüdiger  26  TRUE
```

## rbind

Mit `rbind` können wir eine weitere Zeile (row) hinzufügen.

```r
rbind(df,
      c("Lennard", 38, TRUE))
```

```
     name age child
1    Marc  25 FALSE
2  Thomas  23  TRUE
3    Leon  26 FALSE
4 Rüdiger  26  TRUE
5 Lennard  38  TRUE
```

## cbind

```r
cbind(df,
      "height" = c(185, 175, 178, 182))
```

```
     name age child height
1    Marc  25 FALSE    185
2  Thomas  23  TRUE    175
3    Leon  26 FALSE    178
4 Rüdiger  26  TRUE    182
```

Vergesse nicht den alten Wert von `df` zu überschreiben oder
in einer neuen Variable zu speichern, sonst geht all unsere
Arbeit verloren.
