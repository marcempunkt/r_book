# Indexing

Wie Matrizen sind auch DataFrames zwei-dimensional,
also benutzen wir hier auch zwei Zahlen innerhalb der `[]` Klammern.

Lass uns zunächst den DataFrame erstellen:

```r
name <- c("Marc", "Thomas", "Leon", "Rüdiger")
age <- c(25, 23, 26, 26)
child <- c(FALSE, TRUE, FALSE, TRUE)

df <- data.frame(name, age, child)
```

```
     name age child
1    Marc  25 FALSE
2  Thomas  23  TRUE
3    Leon  26 FALSE
4 Rüdiger  26  TRUE
```

Wie bei Matrizen, um das Alter von Leon zu bekommen,
geben wir innerhalb der `[]` Klammern erst die Zeile (row)
und dann die Spalte (column) ein.

```r
df[3, 2]
```

```
[1] 26
```

Wir können aber auch den Namen der Spalte verwenden, anstelle einer Zahl:

```r
df[3, "age"]
```

```
[1] 26
```

Mit Vektoren als Indexe können wir auch mehrer Zeilen und Spalten bekommen.
Im Falle von DataFrames haben wir auch die Möglichkeit einen Vektor mit 
den Namen für die Spalten zu verwenden.

```r
df[c(2, 4), c("name", "child")]
```

```
     name child
2  Thomas  TRUE
4 Rüdiger  TRUE
```

TODO

```
df[3,]
df[3]
df[,3]
df[,"child"]
```

Ähnlich wie bei Listen können wir auch bei DataFrames `$` und `[[]]` benutzen,
um alle Werte einer Spalte (column) als Vektor zu bekommen.

```r
df$name       # Console: [1] "Marc" "Thomas" "Leon" "Rüdiger"
df[["name"]]  # Console: [1] "Marc" "Thomas" "Leon" "Rüdiger" 
df[[1]]       # Console: [1] "Marc" "Thomas" "Leon" "Rüdiger"
```
