# Mathe mit Vektoren

Mit Vektoren zu rechnen ist genauso einfach wie im [Kapitel 5.1. Number](number.md).

Plus `+`, Minus `-`, Geteilt `/`, Mal `*`, Hoch `^`, Modulus `%%` alles können wir auch mit
zwei oder mehreren Vektoren benutzen. 

```r
a <- c(1, 2, 3)
b <- c(4, 5, 6)
```

#### Plus +

```r
a + b  # Console: [1] 5 7 9
```

#### Minus -

```r
a - b  # Console: [1] -3 -3 -3
```

#### Geteilt /

```r
a / b  # Console: [1] 0.25 0.40 0.50
```

#### Mal * 

```r
a * b  # Console: [1] 4 10 18
```

#### Hoch ^

```r
a ^ b  # Console: [1] 1 32 729
```

#### Modulus %%

```r
b %% a  # Console: [1] 0 1 0
```

Siehe im [Kapitel 5.1](./number.md) wie der Modulus Operator funktioniert.

## Rechnen mit Zahlen

Wir können auch ohne Probleme einen Vektor plus, minus, geteilt, usw. einer Zahl nehmen, 
dann wird jede Zahl im Vektor mit der anderen Zahl verrechnet.

```
a + 2  # Console: [1] 3 4 5
```

```
a / 2  # Console: [1] 0.5 1.0 1.5
```


## Rechnen mit unterschiedlich langen Vektoren

Wenn wir zwei Vektoren, die unterschiedlich lang sind, miteinander verrechnen,
dann passiert etwas komisches:

```r
c(1, 2, 3, 4) + c(1, 2)  # Console: [1] 2 4 4 6
```

Was wir hier sehen können ist, dass R automatisch den zweiten Vektor verlängert hat.
Aus `c(1, 2, 3, 4) + c(1, 2)` wurde `c(1, 2, 3, 4) + c(1, 2, 1, 2)`.

Wenn wir zwei unterschiedlich lange Vektoren in R miteinander verrechnen, sei es plus, minus, 
geteilt usw., dann wird automatisch der kürzere Vektor verlängert damit beide Vektoren
gleich lang sind.

Dabei muss einer der beiden Vektoren das gerade Vielfache des anderen sein,
sonst bekommen wir einen Error:

```r
vec1 <- c(1, 2, 3)
vec2 <- c(1, 2, 3, 4)
vec1 + vec2
```

```
Warning message:
In vec2 + vec1 :
  longer object length is not a multiple of shorter object length
```

## Einfache Zahl vs. Zahlen-Vektor

Schauen wir einmal hinter den Kulissen, warum sich Zahlen und Vektoren bei mathematischen
Berechnungen nicht unterscheiden.

In R ist jeder Wert und jede Variable, die wir erstellen gleichzeitig auch ein Vektor. 
Zum Beispiel wenn wir eine Variable erstellen mit der Zahl 420, dann haben wir auch
einen Vektor mit nur einem Wert erstellt.

```r
x <- 420
is.numeric(x)  # Console: [1] TRUE
is.vector(x)   # Console: [1] TRUE
```

Die Funktion `is.numeric` und `is.vector` zeigt uns, dass `x` eine Zahl und auch ein Vektor ist.
Jetzt wo wir wissen, dass `x` ein Vector ist, können wir doch bestimmt auch die `[]` Klammern
benutzen, oder nicht? Richtig!

```r
x[1]  # Console: [1] 420
x[2]  # Console: [1] NA
```

Und wenn wir im Kopf behalten, dass R automatisch versucht die Länge des kürzeren Vektors dem anderen
anzupassen, bekommen wir ein Einblick wie R `c(1, 2, 3) + 1` hinter den Kulissen berechnet wird:

```r
# Aus:
c(1, 2, 3) + 1

# wird:
c(1, 2, 3) + c(1, 1, 1)
```

## Übung: Rechnen mit Vektoren

Ich habe meine gesamten Einnahmen und Ausgaben im letzen Jahr aufgezeichnet 
und möchte jetzt berechnen,
ob ich über meine Verhältnisse gelebt habe oder doch etwas sparen konnte.

| Monat     | Einnahmen | Ausgaben |
|-----------|-----------|----------|
| Januar    | 1450      | 1200     |
| Februar   | 1300      | 1100     |
| März      | 1500      | 1800     |
| April     | 1450      | 2000     |
| Mai       | 1450      | 1000     |
| Juni      | 1500      | 1895     |
| Juli      | 1600      | 1500     |
| August    | 1400      | 2000     |
| September | 1450      | 1800     |
| Oktober   | 1450      | 1200     |
| November  | 1500      | 1200     |
| Dezember  | 2100      | 1500     |

Lass uns diese Tabelle in R-Code übersetzen:

```r
income <- c(1450, 1300, 1500, 1450, 1450, 1500, 
            1600, 1400, 1450, 1450, 1500, 2100)
expenses <- c(1200, 1100, 1800, 2000, 1000, 1895, 
              1500, 2000, 1800, 1200, 1200, 1500)
```

Um jetzt herauszufinden in welchen Monat ich plus oder minus gemacht habe, 
kann ich `income` - `expenses` nehmen.

```
income - expenses  # Console: [1] 250 200 -300 -550 450 -395 100 -600 -350 250 300 600
```

### sum

Das gibt mir schonmal einen groben Eindruck, 
ob ich plus oder minus im letzten Jahr gemacht habe,
aber wie viel genau? 
Die `sum` Funktion gibt uns die Summe aller Werte eines numerischen Vektors:

```
sum(income - expenses)  # Console: -45
```

Ich habe wohl doch nicht so gut gespart, wie ich erst gedacht habe...

### median

Mit der `median` Funktion können wir herausfinden 
was der Durchschnitt meiner Einnahmen und Ausgaben war.

```r
median(income)    ## Console: [1] 1450
median(expenses)  ## Console: [1] 1500
```

Wie wir sehen habe ich letztes Jahr im Durchschnitt pro Monat 1450 EUR verdient,
aber 1500 EUR ausgegeben...
