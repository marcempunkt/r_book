# Filtering

Das Filtern von Matrizen funktioniert genau gleich wie das Filtern von Vektoren in
[Kapitel 5.4.3. Filtering](vector_filtering.md).
Wenn wir aber eine Matrix filtern, dann bekommen wir einen neuen Vektor:

```r
a <- matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9), 3, 3)
```

Output von `a`:

```
     [,1] [,2] [,3]
[1,]    1    4    7
[2,]    2    5    8
[3,]    3    6    9
```

Wir filtern nur die ungeraden Zahlen aus der Matrix `a` heraus:

```r
a[a %% 2 == 0]  # Console: [1] 2 4 6 8
```

`a[a %% 2 == 0]` gibt uns einen Vektor, welches nur aus den geraden 
Zahlen der Matrix `a` besteht.

Wir können auch den `%in%` Filter-operator benutzen:

```r
a[!a %in% c(7, 8, 9)]  # Console: [1] 1 2 3 4 5 6
```
