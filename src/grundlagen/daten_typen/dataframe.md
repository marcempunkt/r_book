# DataFrame

Ich habe schon im [Kapitel 5.5. Matrix](matrix.md) gesagt, dass Matrizen wie Excel Tabellen sind,
aber die tatsächlichen Excel Tabellen sind DataFrames. 

DataFrame ist ein mächtiger Datentyp in R, der sehr oft für Datenanalyse benutzt wird.
In DataFrames können wir etliche Daten in etlichen Kategorien in etlichen Datentypen speichern,
um daraufhin Grafen oder Statistiken erstellen zu können.

| Item         | Price | In stock | Inhouse-brand |
|--------------|-------|----------|---------------|
| Juicy Apples | 1.99  | 7        | true          |
| Bananas      | 1.89  | 52       | false         |
| ...          | ...   | ...      | ...           |

Um einen DatenFrame zu erstellen, benutzt man die `data.frame` Funktion:

```r
name <- c("Marc", "Thomas", "Leon", "Rüdiger")
age <- c(25, 23, 26, 26)
child <- c(FALSE, TRUE, FALSE, TRUE)

df <- data.frame(name, age, child)
```

Der Output von der Varibale `df` sieht wie folgt aus:

```
     name age child
1    Marc  25 FALSE
2  Thomas  23  TRUE
3    Leon  26 FALSE
4 Rüdiger  26  TRUE
```

Die `data.frame` Funktion erkennt automtisch anhand der Variablennamen,
die wir innerhalb der `()` Klammern schreiben, 
dass die 3 Spalten die Name: `name`, `age`, `child` haben sollen,
aber wir können die Namen der Spalten auch selber bestimmen:

```r
df <- data.frame("Vorname" = name,
                 "Alter" = age,
                 "Hat_Kinder" = child)
```

```
  Vorname Alter Hat_Kinder
1    Marc    25      FALSE
2  Thomas    23       TRUE
3    Leon    26      FALSE
4 Rüdiger    26       TRUE
```
