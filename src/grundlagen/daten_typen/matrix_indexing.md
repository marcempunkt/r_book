# Indexing

Matrizen sind eigentlich nichts anderes als Vektoren, aber nur zwei-dimensional.
Sprich, um ein Wert von einer Matrix zu bekommen, 
schreibt man zwei Zahlen in die `[]` Klammern. 
Die erste Zahl für die Zeile (row) und die zweite Zahl für die Spalte (column).

```r
x <- matrix(data = 1:16, 
            nrow = 4, 
            ncol = 4)

# x[row, column]
x[1, 2]  # Console: [1] 3
```

Um eine komplette Zeile zu bekommen, lässt du einfach die zweite Zahl leer.
Du musst aber trotzdem das Komma stehen lassen:

```r
x[1,]  # Console: [1] 1 3
```

Um eine komplette Spalte zu bekommen, machst du das selbe nur umgedreht:

```r
x[,1]  # Console: [1] 1 2
```

Wie im [Kapitel 5.4.1 Indexing](vector_indexing.md) von Vektoren besprochen, 
kann man auch eine Range oder
einen Vektor in die `[]` Klammern schreiben, um, in diesen Fall, mehrere Zeilen oder Spalten
aufeinmal zu bekommen.

So bekommen wir Zeile 3 und 4 mit allen Spalten:

```r
x[3:4,]
```

```
     [,1] [,2] [,3] [,4]
[1,]    3    7   11   15
[2,]    4    8   12   16
```

Und so bekommen wir die Spalte 3 und 4 mit allen Zeilen:

```r
x[,3:4]
```

```
     [,1] [,2]
[1,]    9   13
[2,]   10   14
[3,]   11   15
[4,]   12   16
```

Genauso wie bei Vektoren, können wir auch mit einer Minuszahl Zeilen oder Spalten ausschließen.

```r
x[-4, -4]
```

```
     [,1] [,2] [,3]
[1,]    1    5    9
[2,]    2    6   10
[3,]    3    7   11
```

Zu guter letzt zeige ich dir noch wie du Werte einer Matrix überschreiben kannst.

Um einen einzige Stelle zu überschreiben:

```r
x[1, 1] = 420
```

```
     [,1] [,2] [,3] [,4]
[1,]  420    5    9   13
[2,]    2    6   10   14
[3,]    3    7   11   15
[4,]    4    8   12   16
```

Oder eine komplette Reihe:

```r
# Eine komplette Zeile überschreiben:
x[1,] = c(420, 420, 420, 420)

# Eine komplette Spalte überschreiben:
x[,4] = c(68, 68, 68, 68)
```

```
     [,1] [,2] [,3] [,4]
[1,]  420  420  420   68
[2,]    2    6   10   68
[3,]    3    7   11   68
[4,]    4    8   12   68
```
