# Character

Der Datentyp von Text wird in R auch `character` genannt. 
Viele Programmiersprachen benutzen dafür das Wort `string` 
(die englische Übersetzung von "Zeichenkette"). 
Damit der R-Interpreter Text auch als `character` erkennen kann, 
müssen wir den Text mit `"` oder `'` einkapseln.

Beispieltext mit `"`:

```r
greetings <- "Hello"
```

oder mit `'`:

```r
greetings <- 'Hello'
```

aber, wenn wir die `"` oder `'` vergessen, dann bekommen einen Error:

```r
greetings <- Hello  ## Console: Error: object 'Hello' not found
```

R denkt, dass `Hello` eine Variable ist, aber natürlich gibt es `Hello` als Variable
nicht, weswegen wir den Error `object 'Hello' not found` bekommen.

Ein `character` kann auch ein Satz sein, ergo Leerzeichen innerhalb der `"` oder `'` sind erlaubt!

```
greetings <- "Hello world!"
```

Wie dir vielleicht aufgefallen ist, haben wir die `"` Syntax auch schon beim 
`print` oder `switch` Befehl benutzt.

### Wichtige Funktionen für Characters 

Für Characters gibt es viele nützliche Funktionen. Zum Beispiel:

#### paste

Der `paste` Befehl tut mehrere Characters miteinander verbinden.
Innerhalb der `()` Klammern können wir so viele Textsnippets reinpacken wie wir wollen,
aber an letzter Stelle brauchen wir ein `sep=""`, um `paste` zu sagen wie die `Characters`
zusammengeklebt werden sollen.
Bei Default ist `sep` ein Leerzeichen. Wenn ein Leerzeichen für dein Fall okay ist,
dann kannst du es sogar weg lassen.

```r
paste("Mann", "Bär", "Schwein")               ## Console: [1] "Mann Bär Schwein"
paste("Mann", "Bär", "Schwein", sep="")       ## Console: [1] "MannBärSchwein"
paste("Mann", "Bär", "Schwein", sep="-")      ## Console: [1] "Mann-Bär-Schwein"
paste("Mann", "Bär", "Schwein", sep=" und ")  ## Console: [1] "Mann und Bär und Schwein"
```

#### substr

Der `substr` Befehl tut einen Teil eines Characters herausschneiden, anhand zwei Zahlen für den Anfangs- und Endpunkt.

```r
animal <- "Mannbärschwein"
substr(x=animal, start=5, stop=7)  ## [1] "bär"
```

Hier eine Beschreibung der Paramter

| Parameter | Datentyp  | Beschreibung                                             |
|-----------|-----------|----------------------------------------------------------|
| x         | character | Der Character von dem du einen Teil rausschneiden willst |
| start     | number    | Anfangspunkt                                             |
| stop      | number    | Endpunkt                                                 |

#### sub

Mit dem `sub` Befehlt können wir den ersten Text, der dem `pattern` matched, 
innerhalb eines Characters austauschen bzw. ersetzen.

```r
animal <- "Mannbärschwein"
sub(x=animal, pattern="schwein", replacement="gans")  # Console: [1] "Mannbärgans"
```

| Parameter   | Datentyp  | Beschreibung                                       |
|-------------|-----------|----------------------------------------------------|
| x           | character | Input                                              |
| pattern     | character | Character der ausgetauscht werden soll innerhalb x |
| replacement | character | Ersatztext der `pattern` ersetzen soll             |

#### gsub

Wenn wir mehrer Vorkommnise vom `pattern` haben und du willst alle mit einem neuen Text austauschen,
dann musst du `gsub` verwenden. Das tolle an `gsub` ist, dass es von der Benutzung her genauso ist wie `sub`.

```r
gsub(x="Chicken Chicken Goose", pattern="Chicken", replacement="Goose")  # Console: [1] "Goose Goose Goose"
```

## Übungsaufgaben 

1. Weise einer Variable einen Text zu und gib ihn aus.

2. Verbinde zwei Strings miteinander.

