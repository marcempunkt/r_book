# Filtering

Innerhalb der `[]` Klammern nach einem Vektor können wir auch eine logische Aussage eintippen.

Was bedeutet das überhaupt? Nun wenn wir zum Beispiel einen numerischen Vektor von 1 bis 100 haben,
aber wir nur die Werte von den geraden Indexen haben wollen und alle ungeraden rausfiltern wollen, dann können wir
innerhalb der `[]` Klammern eben diese logische Abfrage "ist Zahl gerade?" eingeben.

Kannst du dich noch erinnern, wie wir erkennen konnten, ob eine Zahl gerade oder ungerade ist? Ein kleiner Tipp, 
bevor wir zur Lösung kommen, wir brauchen den `%%` Modulus Operator.

```r
irgendeine_zahl %% 2 == 0  # [1] TRUE oder [1] FALSE 
```

Wenn der Rest durch zwei einer Zahl gleich null ist, dann wissen wir, 
dass die Zahl durch zwei teilbar ist und somit eine gerade Zahl sein muss.

Wie wir gelernt haben im [vorherigen Kapitel](vector_maths.md) 
können wir `irgendeine_zahl` auch ohne Probleme mit einem Vektor austauschen!

```r
one_hundred <- 1:100
one_hundred %% 2 == 0  # [1] FALSE TRUE FALSE ...
```

`%% 2` wird jetzt für jeden einzelnen Wert im Vector ausgeführt und wir überprüfen
ob das Ergebnis gleich null ist. Wenn ja, dann bekommen wir `TRUE` und wenn nicht, dann bekommen
wir `FALSE`, somit haben wir am Ende einen Vektor, der nur aus boolischen Werten besteht.

Aber wie hilft uns das jetzt beim filtern eines Vektors weiter?
Eben diesen "boolischen" Vektor können wir jetzt in die `[]` Klammern schreiben, 
um alles rauszufiltern, was an dem jeweiligen Index `FALSE` ist.

```r
numbers <- 1:100
even_numbers <- numbers[numbers %% 2 == 0]
even_numbers  # [1] 2 4 6 8 10 12 14 16 18 20 ...
```

Wie wir erkennen können, bedeutet `TRUE`, behalte den Wert an diesem Index und `FALSE` bedeutet, 
verwerfe den Wert an diesem Index.
Das selbe Prinzip wie im [Kapitel -](), wo wir mit Hilfe einer Minuszahl Werte ausschließen konnten,
so können wir mit einem Boolean-Vektor Werte in einem anderen Vektor ausschließen.

## Polizeihund Beispiel

Wenn du es bis jetzt nicht wirklich verstanden hast, wie das Filtern mit Vektoren funktioniert, keine Sorge,
du bist definitiv nicht alleine. Deswegen versuche ich es nochmal bildlischer zu beschreiben.

Stell dir vor du bist Polizist und hast einen Polizeihund. Der Polizeihund ist trainiert eine Gruppe von
Menschen zu erschnüffeln. Wie er diese Gruppe erkennen kann, müssen wir im vorher erklären. 
Unser Polizeihund ist zufällig auch ein Mathegenie und kann erkennen ob Zahlen größer oder kleiner sind.
Perfekt! Einen besseren Hund hätten wir uns nicht vorstellen können. Unser Übeltäter ist über 40 Jahre alt.
In der Gruppe haben wir insgesamt 100 Personen. Jede dieser Personen ist im Alter von 1-100 und jedes Alter
gibt es nur einmal. 

In R-Sprache ausgedrückt haben wir eine `range` von `1:100`. Wir wissen der Übeltäter ist `>40` Jahre alt.
Unserem Polizeihund müssen wir jetzt nur die Gruppe, also den `Vector` mit der logischen Aussage überreichen.

```r
gruppe <- 1:100
gruppe > 40  # Console: [1] FALSE FALSE FALSE FALSE ... TRUE TRUE ...
```

Unser Polizeihund geht jetzt reih um und sagt `TRUE` oder `FALSE` bei jeder Person, die über 40 ist.
Ja unser Polizeihund ist nicht nur ein Mathegenie, sondern kann auch reden. Wir sind mitgegangen
und haben eine chronologische Liste gemacht, an welcher Position unser Hund `TRUE` und an welcher Stelle
er `FALSE` gesagt hat. Diese Liste bringt uns nichts. Sie ist zu schwer zu lesen und unsere Kollegen
werden keine Ahnung haben, was die Bedeutung dieser ist. Zum Glück haben einen hoch talentierten
Praktikanten, der für uns noch einmal durch die Gruppe an Menschen geht und jeden nach Hause schickt,
den unser Hund als `FALSE` identifiziert hat. Damit unser Praktikant auch richtig arbeiten kann,
müssen wir ihm die Gruppe als auch die Liste vom Polizeihund überreichen:

```r
gruppe <- 1:100
police_dog_list <- gruppe > 40  # Console: [1] FALSE FALSE FALSE ... TRUE TRUE ...
gruppe[police_dog_list]         # Console: [1] 41 42 43 44 45 ...
```

Wir können uns eine Zeile Code sparen, indem wir den Praktikanten und Polizeihund gleichzeitig arbeiten lassen:

```r
gruppe <- 1:100
gruppe[gruppe > 40]  # Console: [1] 41 42 43 44 45 ...
```

Et voliá. Um sicher zu gehen, dass du das Konzept mit den Polizeihund auch richtig verstanden hast,
probiere es mit anderen Vektoren aus und gebe dem "Polizeihund" auch andere Aussagen, nach dem er und
der Praktikant filtern können. 
Probiere auch andere logische Operatoren aus, 
die wir im [Kapitel 5.2.1](logische_operatoren.md) gelernt haben!

## Filtern mit %in%

Der `%in%` Operator wird dann verwendet, wenn wir genau wissen, wonach was wir suchen.

Zum Beispiel, anstelle dass unser Polizeihund nach einer logischen Aussage filtert, 
geben wir ihm jetzt eine Liste mit den Namen der Verbrecher.
Unser Polizeihund geht jetzt über eine Liste an Personen und schickt alle Heim, 
die nicht auf der Vebrecherliste stehen.

```r
group_of_people <- c("Marc", "Michael", "Niklas", "Paul", "Andre")
suspects <- c("Marc", "Paul")
group_filtered_by_suspects <- group_of_people[group_of_people %in% suspects]

group_filtered_by_suspects # Console: [1] "Marc" "Paul"
```

Aus unserer Gruppe an Personen `group_of_people` holen wir uns nur diejenigen heraus, 
die auch im Vektor `suspects` vermerkt sind.

Oft müssen wir aber auch das Gegenteil machen, also wir wollen bestimmte Werte 
rausfiltern bzw. entfernen. Wenn wir den `!` nicht Operator mit den `%in%` Operator kombinieren,
können wir bestimmte Werte von einem Vektor entfernen.

```r
invitations <- c("Thomas", "Bernd", "Lukas", "Joshua")
declined <- c("Joshua", "Bernd")
invitations <- invitations[!invitations %in% declined]

invitations  # Console: [1] "Thomas" "Lukas"
```

Hier habe ich eine Liste an Leuten, denen ich eine Einladung zur meiner Party geschickt habe,
aber leider haben Joshua und Bernd abgesagt und deshalb werde ich sie aus meiner Eingeladenen
(`invitations`) Liste entfernen, um ein besser Plan zu haben, wer alles kommt und wer nicht.

`invitations %in% declined` gibt mir einen Vektor, der nur aus `TRUE` und `FALSE` besteht.
Diesen Vektor tue ich dann mit `!` umkehren, also alle `TRUE`s werden `FALSE` und umgedreht.
Aufgrund diesen `TRUE`/`FALSE` Vektor kann ich nun `invitations` filtern und mit mit dem
neu-gefilterten Vektor den alten überschreiben.

Gehen über noch ein weiteres Beispiel mit `%in%`.

Wir haben von einer Schule eine Liste mit den Alter aller Schüler bekommen.
Bei all denen, die nicht ihr Alter angegeben haben, wurde eine `0` eingetragen.
Mit den `%in%` Operator können wir jetzt all diese Nullen rausfiltern.

```r
ages <- c(24, 18, 19, 20, 34, 0, 17, 19, 0, 39)
ages <- ages[!ages %in% 0]

ages  # Console: [1] 24 18 19 20 34 17 19 39
```
