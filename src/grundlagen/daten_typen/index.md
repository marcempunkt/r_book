# Datentypen

Neben Zahlen können wir auch Variablen für Text, Listen, Tabellen und 
viele, viele andere Dinge erstellen.

In diesen Kapitel gehen wir Schritt für Schritt über die wichtigsten Datentypen von R und
wie man mit diesen arbeiten kann.

Hier schonmal eine kleine Übersicht von all den Datentypen, 
die ich Dir näher bringen werde:

| Datentyp  | Ein-Wort-Erklärung      |
|-----------|-------------------------|
| Number    | Zahl                    |
| Boolean   | Wahr oder Falsch        |
| Character | Text                    |
| Vector    | Liste an Werten         |
| Matrix    | zwei-dimensionale Liste |
| List      | Multityp-liste          |
| Factor    | Kategorien              |
| DataFrame | Tabelle                 |
