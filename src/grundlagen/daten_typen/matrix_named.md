# Named Matrices (Benannte Matrizen)

Genauso wie wir es bei Vektoren gelernt haben oder es auch Excel Tabellen
möglich ist, können wir "Tabellen" erstellen, deren Zeilen und Spalten
benannt sind.

Das ist vorallem sehr interessant, wenn wir mit komplexen Daten zu tun haben,
wie zum Beispiel einen Notenspiegel:

```r
# Definiere die Matrix-Daten
point_matrix <- matrix(c(85, 90, 88, 92, 78, 80), 
                       nrow = 2, 
                       ncol = 3, 
                       byrow = TRUE)

# Definiere die Zeilen- und Spaltennamen
rownames(point_matrix) <- c("Alice", "Bob")
colnames(point_matrix) <- c("Math", "Science", "English")

point_matrix
```

```
      Math Science English
Alice   85      90      88
Bob     92      78      80
```

Ich hoffe dieses Beispiel hilft, um zu verstehen wie man "named matrices"
benutzen könnte, aber für solche Fälle werden meist DataFrames bevorzugt,
aber keine Sorge wir werden in [Kapitel 5.8 DataFrame](dataframe.md) 
alles über DataFrames lernen!
