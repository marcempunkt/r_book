# Logische Operatoren

Logische Operatoren sind spezielle Symbole, die in der Programmierung verwendet werden, 
um logische Vergleiche zwischen zwei oder mehreren Ausdrücke zu machen.

Uff, die Erklärung war viel zu kompliziert, nicht wahr?

In anderen Worten: ein Logischer Operator ist wie eine Behauptung, die entweder wahr oder falsch sein kann.
Wenn ich behaupte, dass die Corona Impfung ein Versuch von Bill Gates sei, 
die Menschheit zu unterjochen, dann ist die Aussage entweder `TRUE` oder `FALSE`.  
Obwohl wenn man auf Facebook geht, es für manche Menschen `TRUE` ist...

Hier eine Tabelle von allen Logischen Operatoren, die es in R gibt.
Einige werden dir von der Mathematik bekannt sein:

| Operator | Bedeutung      |
|----------|----------------|
| &&       | und            |
| \|\|     | oder           |
| ==       | gleich         |
| !=       | nicht gleich   |
| !        | nicht          |
| <        | kleiner als    |
| >        | größer als     |
| <=       | kleiner gleich |
| >=       | größer gleich  |

Aber keine Sorge, ich gehe über jeden Operator und gebe dir ein Beispiel, 
wie man es benutzen könnte.

#### && und

```r
T && F ## Console: [1] FALSE
```

Der "und"-Operator überprüft ob die beiden Werte, links und rechts, `TRUE` sind.

Zum Beispiel wir wollen überprüfen ob ein Kunde, der Alkohol kaufen möchte, über 18 ist
und einen Ausweis mit sich trägt. Nur wenn beides der Fall ist, können wir ihm im 
guten Gewissen Alkohol verkaufen:

```r
is_adult <- TRUE
has_passport <- FALSE

is_adult && has_passport ## Console: [1] FALSE
```

Aber wenn ein Kunde unseren Onlineshop besucht und beides erfüllt, können wir endlich
Profit mit unserem Alk machen.

```r
is_adult <- TRUE
has_passport <- TRUE

is_adult && has_passport ## Console: [1] TRUE 
```

#### || oder

```r
T || F ## Console: [1] TRUE
```

Der "oder"-Operator überprüft ob links oder rechts `TRUE` ist. 
Nur eine der beiden Seiten muss `TRUE` sein, damit die Aussage als `TRUE` gilt.

Zum Beispiel in einer Dating App möchte ich mit Personen gematched werden,
die entweder Hunde oder Katzen mögen.

```r
likes_cats <- TRUE
likes_dogs <- FALSE

likes_cats || likes_dogs ## Console: [1] TRUE
```

#### == gleich 

```r
T == F ## Console: [1] FALSE
```

Der "gleich"-Operator überprüft ob die linke Seite identisch mit der rechten Seite ist.
Wir können auch Zahlen und alle Datentypen, die wir noch lernen werden, damit vergleichen.

```r
my_age <- 25
your_age <- 28

my_age == your_age ## Console: [1] FALSE

your_age <- 25 ## Überschreibe your_age mit 25

my_age == your_age ## Console: [1] TRUE
```

#### != nicht gleich

```r
T != F ## Console: [1] TRUE
```

Das selbe wie beim "gleich" können wir auch hier machen, aber umgedreht mit dem "nicht-gleich"-Operator.
Dieser checkt ob die linke Seite nicht gleich mit der rechten Seite ist.

```r
my_age <- 25
your_age <- 28

my_age != your_age ## Console: [1] TRUE

your_age <- 25 ## Überschreibe your_age mit 25

my_age != your_age ## Console: [1] FALSE
```

#### ! nicht

```r
!T ## Console: [1] FALSE
```

Der "nicht"-Operator tut einen "boolischen" Wert (`TRUE` oder `FALSE`) umdrehen. 
So wie wir es oben im Beispiel sehen können, wo `TRUE` mit `!` zu `FALSE` umkonvertiert wird. 
Schauen wir uns weitere Beispiel des "nicht"-Operator an!

```r
!TRUE  ## Console: [1] FALSE
!T     ## Console: [1] FALSE
!FALSE ## Console: [1] TRUE
!F     ## Console: [1] TRUE
!42    ## Console: [1] FALSE
!0     ## Console: [1] TRUE
!-42   ## Console: [1] FALSE
```

Du fragst dich jetzt bestimmt warum `!0` `TRUE` ist und warum `!42` sowie `!-42` `FALSE` ist.
In vielen Programmiersprachen wird die Zahl 0 als `FALSE` behandelt, 
während alle anderen Zahlen (sowohl positive als auch negative) als `TRUE` behandelt werden.

Nun, da alle anderen Zahlen (außer 0) als `TRUE` gelten, sind sowohl 42 als auch -42 wahr. 
Wenn wir den "nicht"-Operator auf 42 und -42 anwenden, kehren wir ihre Werte um. Daher werden sowohl !42 als auch !-42 zu `FALSE`.

Wir können auch sebstverständlich den "nicht"-Operator an Variablen verwenden.

```r
is_r_cool <- FALSE
!is_r_cool  ## Console: [1] TRUE
```

#### < kleiner als

```r
42 < 24 ## Console: [1] FALSE
```

Das "kleiner als"-Symbol wird verwendet, um zwei Werte miteinander zu vergleichen. 
Wenn der Wert auf der linken Seite des Symbols kleiner ist als der Wert auf der rechten Seite, 
dann ist die Aussage `TRUE` oder andernfalls `FALSE`.

#### > größer als

```r
42 > 24 ## Console: [1] TRUE
```

Das selbe auch beim "größer als"-Symbol.

#### <= kleiner gleich

```r
42 <= 24 ## Console: [1] FALSE
```

Das "kleiner gleich"-Symbol tut nicht nur `TRUE` geben, wenn die linke Seite kleiner ist als die rechte,
sondern auch wenn die linke Seite gleich der rechten Seite ist.

```r
42 <= 43  ## Console: [1] TRUE
42 <= 42  ## Console: [1] TRUE
42 <= 41  ## Console: [1] FALSE
```

#### >= größer gleich

```r
42 >= 24 ## Console: [1] TRUE
```

Das selbe auch beim "größer gleich"-Symbol.

```r
42 >= 43  ## Console: [1] FALSE
42 >= 42  ## Console: [1] TRUE
42 >= 41  ## Console: [1] TRUE
```

Im nächsten Kapitel werden wir all diese Operatoren bzw. Symbole verwenden, 
um mit Hilfe von `if` und `else` in unserem Programm Entscheidungen treffen
zu können.
