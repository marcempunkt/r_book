# Matrix

Matrizen sind zwei-dimensionale Vektoren. 
Stell dir eine Matrix wie eine Excel Tabelle mit Zeilen (rows) und
Spalten (columns) vor.
Genau wie bei Vektoren, können auch Matrizen nur einen Datentyp in sich tragen.

Mit der `matrix()` Funktion können wir eine neue Matrix erstellen.
Innerhalb der `()` Klammern kommt ein Vektor mit den Daten für die Matrix.

```r
x <- matrix(c(1, 2, 3, 4))
x
```

Dadurch haben wir eine ein-spaltige Matrix mit 4 Zeilen erstellt:

```
     [,1]
[1,]    1
[2,]    2
[3,]    3
[4,]    4
```

Wir können auch ohne Probleme eine Matrix mit characters 
(booleans oder jeden anderen beliebigen Datentyp) erstellen:

```r
y <- matrix((c("a", "b", "c", "d")))
y
```

```
     [,1]
[1,] "a" 
[2,] "b" 
[3,] "c" 
[4,] "d" 
```

Wir können auch bestimmen wie viele Spalten (columns) und Zeilen (rows) 
unsere Matrix haben soll.

```r
x <- matrix(c(1, 2, 3, 4), 
            2, 
            2)
x
```

```
     [,1] [,2]
[1,]    1    3
[2,]    2    4
```

Hier haben wir drei Werte der `matrix` Funktion gegeben:  
Die Daten (`data`), die Anzahl der Zeilen (`nrow`) un die Anzahl der Spalten (`ncol`).  
In R können wir jeden dieser Werte, die wir innerhalb einer Funktion schreiben,
auch mit deren richtigen Namen zuweisen:

```r
x <- matrix(data = c(1, 2, 3, 4), 
            nrow = 2, 
            ncol = 2)
```

Was der "richtige Name" ist, können wir herausfinden, indem wir in die R-Konsole ein `?`
gefolgt mit den Funktionsname eintippen. Probiere es einmal aus mit `? matrix`!

Anstelle irgendwelche Zahlen oder Vektoren in den `()` Klammern zu schreiben, 
können wir mit `paramter = x` genau sagen welchen Parameter wir welchen Wert zuweisen.
Wenn wir das so machen, dann ist auch die Reihenfolge egal, 
weil R es automatisch für uns erkennen kann.

## Matrix erstellen `byrow`

Beim erstellen einer Matrix wird **Spalte für Spalte** mit Daten gefüllt.

```r
x <- matrix(data = (c("one", "two", "three", "four")),
            nrow = 2,
            ncol = 2)
x
```

```
     [,1]  [,2]
[1,] "one" "three"
[2,] "two" "four" 
```

Wenn wir das nicht wollen, dann können wir der `matrix` Funktion
auch sagen, dass die Matrix **Zeile für Zeile** befüllt werden soll.

```r
x <- matrix(data = c("one", "two", "three", "four"), 
            nrow = 2, 
            ncol = 2, 
            byrow = TRUE)
x
```

```
     [,1]    [,2]
[1,] "one"   "two" 
[2,] "three" "four"
```
