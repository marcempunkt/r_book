# Named Lists (Benannte Listen)

Wie bei Vektoren oder Matrizen, haben wir auch bei Listen die Möglichkeit Indexe 
einen Namen zu geben:

```r
my_list <- list(numbers=c(1, 2, 3),
                message="R ist geil!",
                is_cool=TRUE)
```

```
$numbers
[1] 1 2 3

$message
[1] "R ist geil!"

$is_cool
[1] TRUE
```

Anstelle Zahlen für den Index können wir auch die Namen,
die wir definiert haben, benutzen:

```r
my_list[["message"]]  # Console: [1] "R ist geil!"
```

Wir können auch die `[[]]` mit einem `$` austauschen:

```r
my_list$message       # Console: [1] "R ist geil!"
```

Das `$` funktioniert aber nur mit dem Namen und nicht mit einem Index.
`my_list$2` würde uns folgenden Error geben:

```
Error: unexpected numeric constant in "my_list$2"
```

Wir können auch eine Liste mit einem neuen Namen erweitern.
Zum Beispiel lass ein weiteren Wert mit den Namen "admin" hinzufügen:

```r
my_list$admin <- "marc"
```

```
$numbers
[1] 1 2 3

$message
[1] "R ist geil!"

$is_cool
[1] TRUE

$admin
[1] "marc"
```
