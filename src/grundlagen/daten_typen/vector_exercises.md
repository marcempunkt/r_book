# Übungsaufgaben

1. Erstelle einen Vektor mit 5 Zahlen und gib ihn aus.

2. Wähle das 3. Element aus einem Vektor aus und gib es aus.

3. Lösche ein Element aus einem Vektor.

4. Kombiniere zwei Vektoren

5. Erstelle einen Vektor mit einer Range von 1 bis 1.000.000.

6. Aus diesen Vektor finde heraus wie viele davon durch 7 teilbar sind. 
(Tipp: Mit `length` bekommt man die Länge eines Vektors)
