# if und else

Mit `if` und `else` können wir innerhalb unseres Programmes Entscheidungen treffen, 
um eine Aktion auszuführen, wenn eine bestimmte Bedingung erfüllt ist 
und eine andere Aktion, wenn die Bedingung nicht erfüllt ist.

```r
number <- 5

if (number %% 2 == 0) {
    print("number is even")
} else {
    print("number is odd")
}
```

Nach dem `if`-Schlüsselwort folgt eine `()`-Klammer. Innerhalb dieser Klammer müssen wir
eine Aussage schreiben, die sich entweder zu `TRUE` oder `FALSE` auflöst.
In der `{}`-Klammer, direkt danach, kommt der Code der ausgeführt wird, wenn die Aussage
`TRUE` ist. Wenn aber die Aussage `FALSE` ist wird die nächste `{}`-Klammer ausgeführt, die
mit `else` markiert worden ist.

Also nochmal einfacher erklärt mit einem Beispiel:

```r
if (this_is_true) {
    do_this()
} else {
    do_that()
}
```

Im Beispiel von ganz oben wird "number is even" in der Console ausgegeben, wenn der Rest von `number`
geteilt durch zwei null ist, also die Zahl gerade ist und wenn dies nicht der Fall ist,
dann wird "number is odd" ausgegeben.

Wir können auch mehrere `if`'s hintereinander reihen um mehrere Bedingungen Schritt für zu überprüfen.

```r
if (this_is_true) {
    do_x()
} else if (or_this_is_true) {
    do_y()
} else {
    do_z()
}
```

Lass uns ein Beispiel schreiben, bei dem in der Console eine Nachricht geschrieben wird, wenn
die Zahl positiv, negativ oder null ist.

```r
number <- -7

if (number < 0) {
  print("Number is negative.")
} else if (number > 0) {
  print("Number is positive.")
} else {
  print("Number is null.")
}
```

Als erstes checken wir ob die Zahl kleiner als null ist, dann printen wir "Number is negative" und
wenn die Zahl größer als null ist, dann printen wir "Number is positive".
Wenn beides nicht der Fall ist, also beide Aussagen `FALSE` sind dann wird der `else` Block ausgeführt und
logisch können wir ab diesen Punkt von ausgehen, dass die Zahl `0` sein muss!

Wir sind aber nicht verpflichtet ein `else` zu verwenden am Ende einer `if`-Kette.

```r
do_x()

if (this_is_true) {
    do_y()
}

do_z()
```

So können wir zum Beispiel erst `x` machen und wenn die `if`-Aussage `TRUE` ist, 
dann machen wir auch `y`, aber wenn es `FALSE` ist, naja, dann machen wir halt `y` nicht 
und ungeachtet der `if` Aussage, machen wir `z` als nächstes.

## Übungsaufgaben

1. Hier ein paar Übungsaufgaben mit `if` und `else`:

2. Teile eine Zahl durch eine andere und prüfe, ob das Ergebnis eine ganze Zahl ist.

3. Überprüfe, ob eine Zahl positiv, negativ oder null ist und gib eine entsprechende Meldung aus.

4. Überprüfe, ob eine Zahl gerade oder ungerade ist

5. Vergleiche zwei Zahlen und gib an, welche größer ist.

6. Überprüfe, ob eine Zahl in einem bestimmten Bereich liegt (z.B. zwischen 10 und 20).

7. Schreibe eine Bedingung, die prüft, ob eine Zahl durch eine andere teilbar ist.

8. Schreibe eine Bedingung, die überprüft, ob eine Zahl ein Vielfaches von 10 ist.

9. Schreibe ein Programm, welches aus einer Punktzahl (0-100) eine Note (1-6) zuweist

10. Erkenne aus 3 Zahlen welche davon die größte ist und welche davon die kleinste ist

11. Empfehle aufgrund des Alters einer Person einen Film
