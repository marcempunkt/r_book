# Factor

Faktoren sind Kategorien, die uns helfen Datenmengen, zu sortieren, in zum Beispiel
Geschlecht, Herkunftsland oder Elektro-/Verbrennermotoren usw. 
Die Möglichkeiten sind endlos, da wir unsere komplett eigenen Faktoren erstellen können.

Einen Faktor erstellt man aus einem Vektor mithilfe der `factor` Funktion:

```r
# Erstelle einen Vektor mit 10 `yes` und 15 `no`
data <- c(rep("yes", 10),
          rep("no", 15))

data_factor <- factor(data)
```

`data_factor` ist nur ein Faktor mit 2 Kategorien, in R auch Levels genannt,
aber wir können uns die Levels auch einmal anschauen:

```r
levels(data_factor)  # Console: [1] "no"  "yes"
```

Wir können aber auch beim erstellen des Faktors die Levels bestimmen:

```
data_factor <- factor(data,
                      levels=c("yes", "no", "maybe"))
                      
levels(data_factor)  # Console: [1] "yes" "no" "maybe"
```

Falls wir im nachhinein unzufrieden sein sollten, können wir auch die Levels überschreiben:

```r
levels(data_factor) <- c("yes", "no", "unknown")

levels(data_factor)  # Console: [1] "yes" "no" "unknown"
```

Oder einen neuen Level hinzufügen:

```r
levels(data_factor) <- c("yes", "no", "unknown", "maybe")

levels(data_factor)  # Console: [1] "yes" "no" "unknown" "maybe"
```

Mit Faktoren können wir dann ganz einfach erkennen, 
wie viele `yes` und `no` wir in unserer `data` haben.
Die `summary` Funktion gibt uns allerlei Informationen 
zu verschiedenen Daten und bei Faktoren bekommen wir wie 
oft ein Level (eine Kategorie) vorhanden ist.

```r
summary(data_factor)
```

```
yes   no  unknown   maybe 
 10   15        0       0 
```

Wenn wir nicht benutzte Level automatisch entfernen wollen, 
dann können wir die `droplevels` Funktion benutzen:

```r
data_factor <- droplevels(data_factor)

levels(data_factor)  # Console: [1] "yes" "no"
```
