# Named Vectors (Benannte Vektoren)

In R haben wir auch dir Möglichkeit jedem einzelnen Wert an einem Index einen
eigenen Namen zu geben. Dies kann uns helfen die Daten, die wir einem Vektor zuweisen,
besser einorden zu können.

Lass uns einen Vektor erstellen, der die Stunden, 
wie viel ich in der Woche gearbeitet habe, hat.

```r
workhours <- c(8, 8, 10, 8, 6, 0, 0)
```

Der Variablenname `workhours` hilft uns dabei zu verstehen, was unser Ziel ist,
aber es wird schwieriger nachzuvollziehen, wenn wir z.B. die Stunden von Freitag
wissen wollen.

```r
workhours[5]  # Console:  [1] 6
```

Wir können jetzt im Nachhinein jedem Index einen eigenen Namen geben!

```
names(workhours) <- c("Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday", "Sunday")
```

Und jetzt können wir, anstelle `workhours[5]` zu schreiben, auch `workhours["Friday"]` schreiben!

Der `names` Funktion können wir einen neuen Vektor zuweisen, der den Namen der jeweiligen Indexen
entsprechen soll. Dies heißt, dass der Name für den Index 1 von workhours jetzt "Monday" ist und
von 2 "Tuesday" usw. In der Documentation der Funktion, die wir mit dem Befehl `? names`
in der R-Konsole bekommen steht folgendes: `Functions to get or set the names of an object.`

Wir müssen aber nicht immer einen `named vector` so erstellen:

```r
workhours <- c(8, 8, 10, 8, 6, 0, 0)
names(workhours) <- c("Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday", "Sunday")
```

Wir können auch schon beim erstellen des `workhours` Vektors Namen für den jeweiligen Index mit geben:

```r
workhours <- c("Monday"=8,
               "Tuesday"=8,
               "Wednesday"=10,
               "Thursday"=8,
               "Friday"=6,
               "Saturday"=0,
               "Sunday"=0)
```

Apropos dies würde auch ohnen den `"` Zeichen klappen:

```r
workhours <- c(Monday=8,
               Tuesday=8,
               Wednesday=10,
               Thursday=8,
               Friday=6,
               Saturday=0,
               Sunday=0)
```

Jetzt können wir in den `[]` Klammern Zahlen oder einer Namen eingeben:

```r
workhours[1]         # Console: Monday 8
workhours["Monday"]  # Console: Monday 8
```

Vergesse aber nicht die `"` Zeichen, weil sonst denkt R, dass `Monday` eine Variable ist
und wenn diese nicht existiert, bekommen wir einen Error.
Japp. R ist sehr eigen, wenn es darum geht, wann " Optional oder Pflicht sind.
