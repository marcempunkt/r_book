# Ordered Factor

Neber Faktoren können wir auch "sortierte" Faktoren erstellen, 
die eine Hierachie oder Gewichtung darstellen können.

Stell dir vor du hast Umfragedaten, auf denen Fragen die einzelnen Befragten 
"very low", "low", "medium", "high" oder "very high" antworten konnten.
Unser menschliches Gehirn sortiert diese Kategorien automatisch ein. 
"very low" ist ganz unten und "very high" dementsprechend ganz oben.

Um auch nach diesem Prinzip Faktoren zu erstellen,
müssen wir beim erstellen den Paramter `ordered` auf `TRUE` setzen:

```r
## Erstelle einen Vektor mit jeweils 5 "very low", "low", "medium", ...
questionnaire <- rep(c("very low", "low", "medium", "high", "very high"), 5)

## Erstelle aus dem `questionnaire`
questionnaire_factor <- factor(questionnaire,
                              levels=c("very low", "low", "medium", "high", "very high"),
                              ordered=TRUE)
```

Wichtig ist es innerhalb der `factor` Funktion nicht nur `ordered` auf `TRUE` zu setzen, 
sondern auch `levels` in der Reihenfolge von niedrig zu hoch setzen.
In diesen Beispiel von "very low" bis "very high".

```r
questionnaire_factor
```

```
Levels: very low < low < medium < high < very high
```

Vom Output, wenn wir `questionnaire_factor` in der R-Konsole eingeben, 
können wir die Einstufung der Gewichtung der Kategorien erkennen.

Zum Vergleich habe ich hier nocheinmal `questionnaire_factor` ohne `ordered = TRUE` erstellt:

```
Levels: very low low medium high very high
```

Faktoren werden vorallem im [Kapitel 8. Plots](../../plots/index.md) 
und [Kapitel 9. ggplot](../../ggplot/index.md) interessant,
wenn es darum geht eigene Grafen zu erstellen.
