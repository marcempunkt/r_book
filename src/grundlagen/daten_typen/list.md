# List

Listen sind wie Vektoren, die in der Lage sind mehrer verschiedene Datentypen aufeinmal
in sich zu tragen. Alles ist in einer Liste erlaubt, sogar Vektoren oder noch mehr Listen.
Egal was, wirklich alles ist erlaubt.

Kannst du es in einer Variable packen?  
Dann darfst du es auch in eine Liste packen!

Um eine neue Liste zu erstellen, brauchen wir die `list` Funktion:

```r
my_list <- list(c(1, 2, 3),
                "R ist cool!",
                420,
                TRUE)
```

Wenn wir `my_list` in der R-Konsole eingeben, dann bekommen wir diesen Output:

```
[[1]]
[1] 1 2 3

[[2]]
[1] "R ist cool!"

[[3]]
[1] 420

[[4]]
[1] TRUE
```

Die Doppel `[[]]` Klammer mit einer Zahl drinne, 
sagt uns am welchen Index dieser Wert in unserer Liste ist.
