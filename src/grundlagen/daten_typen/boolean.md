# Boolean

Den nächsten Datentyp, den wir lernen werden, ist der `Boolean`.

Ein `Boolean` kann nur zwei mögliche Werte haben kann: 
    "wahr" also `TRUE` oder "falsch" also `FALSE`.

Stelle dir einen Boolean wie einen Lichtschalter vor, 
der entweder ein- oder ausgeschaltet sein kann. 
In diesem Fall ist "eingeschaltet" `TRUE` und "ausgeschaltet" `FALSE`.

In der Programmierung sind Booleans nützlich, um Entscheidungen zu treffen oder 
den Ablauf eines Programms zu steuern. 
Zum Beispiel kann man Booleans verwendet, um zu bestimmen, 
ob ein Kunde erlaubt ist von unserem Onlinestore Alkohol zu kaufen
oder ob der User sein Programm in Light oder Dark Theme eingestellt hat.

```r
is_light_theme <- TRUE
user_can_buy_alcohol <- FALSE
```

Anstelle immer `TRUE` oder `FALSE` schreiben zu müssen, 
können wir es auch abkürzen mit `T` oder `F`.

```r
is_r_cool <- T
is_progamming_boring <- F
```

Es gibt überhaupt kein Unterschied zwischen `TRUE` und `T` sowie `FALSE` und `F`. 
Benutze das, was dir besser gefällt.
Ganz nach dem Motto "Whatever floats your boat!"
