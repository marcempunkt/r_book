# Indexing

Wie bei Vektoren, benutzen wir die `[]` Klammern, um einen Wert von einer Liste zu bekommen.

```r
my_list[2]
```

```
[[1]]
[1] "R ist cool!"
```

Was auffällt ist, dass wir etwas mehr als nur `[1] "R ist cool!"` bekommen.
Der Grund hierfür ist, dass wir nicht nur den Text "R ist cool" sondern
eine Liste mit der Länge eins bekommen haben.

```r
typeof(my_list[2])  # Console: [1] "list"
```

Die `typeof` Funktion sagt uns, dass `my_list[2]` von Typ `list` ist.
Um den tatsächlichen Wert zu bekommen, müssen wir zwei `[[]]` Klammern benutzen.

```r
my_list[[2]]          # Console: [1] "R ist cool!"
typeof(my_list[[2]])  # Console: [1] "character"
```

Natürlich können wir auch einen Wert an einem Index überschreiben oder 
einen neuen hinzufügen:

```r
my_list[[5]] <- c(18, 17, 48)
```

```
[[1]]
[1] 1 2 3

[[2]]
[1] "R ist cool!"

[[3]]
[1] 420

[[4]]
[1] TRUE

[[5]]
[1] 18 17 48
```

Oder entfernen, indem wir `NULL`, also nichts, zuweisen:

```r
my_list[5] <- NULL
```

```
[[1]]
[1] 1 2 3

[[2]]
[1] "R ist cool!"

[[3]]
[1] 420

[[4]]
[1] TRUE
```

