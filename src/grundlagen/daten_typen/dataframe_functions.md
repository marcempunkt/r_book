# Wichtige Funktionen

## DataFrame sortieren

Sowie in Excel können wir auch ein df sortieren:

```
sort(people$age) # -> [1] 23 25 26 26 38
order(people$age) # -> [1] 2 1 3 4 5
```
order returnt die indexe der Zeilen sortiert nach dem Alter
um eine sortierte df zu bekommen:
```
people[order(people$age),] ## für aufsteigend nach dem alter
people[order(people$age, decreasing = TRUE),] ## absteigend
```

## dim

```r
dim(customerData) # gette dimensions -> 5 (rows) 4 (columns)
```

## colnames und rownames

```r
colnames(people) ## gette column names
rownames(people) ## gette row names
```

## ncol und nrow

```r
ncol(people) ## Zahl der column
nrow(people) ## Zahl der rows
```

## subset

```r
subset(people, height > 179) ## filtering
people[people$height > 179,] ## macht das selbe wie subset
```

## str und summary

```r
str(people) ## printed "str"ucture eines R-Objektes in diesem Fall
summary(people) ## gibt uns "result summaries"
```

## fivenum
```
fivenum(people) ## Tukey's five nummer summary (minimum, lower-hinge, median, upper-hinge, maximum)
```

## DataFrame als Datei abspeichern

printet es alle wichtigen Infos des people df

```
write.csv(people, "people.csv") ## speicher df als csv Datei auf dem Filesystem ab
write.csv(people, "people.csv", row.names=FALSE) ## um die Zeilennummern zu entfernen
people <- read.csv("people.csv") ## um eine csv Datei als df zu lesen/laden
```

# colSums
