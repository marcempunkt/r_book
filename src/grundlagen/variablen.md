# Variablen

**Variablen**, den Begriff kennen wir schon von der Mathematik. 
Es bedeutet, dass ein Wert variabel ist, also sich verändern kann.
Zum Beispiel die Variable `x` in `y = x + 2` ist ein Platzhalter für einen oder
beliebig vielen Werten.

Auch so sind in der Programmierung Variablen als eine Art "Platzhalter" oder
viel mehr "Zwischenspeicher" für einen Wert, den wir selber bestimmen müssen, zu verstehen.

Aber durch Theorie allein kann man nur schwer lernen oder etwas verstehen, 
also lass uns eine Variable erstellen, die mein jetziges Alter "zwischenspeichert".

```r
my_age <- 25
```

Ich habe hier eine Varible mit den Namen `my_age` erstellt 
(in Programmierfachchinesisch nennt man das auch "deklarieren").

`my_age` bekommt den Wert `25` zugewiesen. 
Um in R eine Variable zu deklarieren, dürfen wir das `<-` nicht vergessen, 
dadurch weiß R überhaupt, dass `my_age` eine Variable ist.
Ohne das `<-` kann der [R-Interpreter](r_und_rstudio/einfuehrung_in_r_interpreter.md) 
unser Code nicht verstehen und
wir würden einen Error bekommen.

Wenn ich jetzt mein Alter wissen möchte
(weil ich es wieder einmal vergessen habe, was mir oft passiert...),
dann kann ich darauf zugreifen, indem
ich in einer späteren Zeile im Skript einfach nur `my_age` schreibe oder 
in der R Konsole `my_age` mit `ENTER` eingebe.

```r
my_age <- 25
my_age # printet "25" in der R Konsole
```

Überall wo wir jetzt `my_age` brauchen, können wir, 
anstelle immer wieder `25` zu schreiben, `my_age` schreiben.
Der Vorteil ist, wenn wir unser Code anpassen möchte für einen anderen User, 
der jünger ist, dann müssen wir nur die Zeile mit `my_age <- 25` anpassen.

Die meisten Programmiersprachen nutzen das `=` Zeichen, 
um Variablen zu deklarieren. Natürlich geht das auch in R:

```r
my_age <- 25
my_age = 25 ## '=' ist das selbe wie '<-'
```

`<-` und `=` sind besondere Zeichen, damit R versteht, dass wir eine Variable erstellen wollen. 
Machen wir einen Fehler und versuchen eine Variable mit `->` zu deklarieren, 
dann bekommen wir einen Error in der R-Konsole.

Wie in einer Fremdsprache, wenn wir Fehler in der Grammatik machen,
kann es sein, dass man uns nicht verstehen kann.
Genau so gibt es auch in Programmiersprachen "Grammatik", 
damit der Computer weiß, was wir machen möchten.
Diese "Grammatik" nennt man in Programmierfachchinesisch auch **Syntax**.
