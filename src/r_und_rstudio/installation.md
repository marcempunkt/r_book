# Installation

Um in R programmieren zu können, müssen wir zwei Programme installieren.
Einmal den R selber und Rstudio.

Rstudio ist eine IDE (Abkürzung für "Integrated development environment").
Du kannst dir eine IDE wie ein Word Programm vorstellen, nur spezialisiert fürs Programmieren.
RStudio bietet uns viele Features an, die es uns einfacher machen werden, für R Code zu schreiben.

### R installieren:

Um R zu installieren, besuche die [R Project Website](https://www.r-project.org/) und
lade die aktuelle Version für dein Betriebssystem herunter.

### RStudio installieren:

Um RStudio zu installieren, besuche die [posit Website](https://posit.co/download/rstudio-desktop/) und
klicke auf den Button "Download RStudio for \<DEIN BETRIEBSSYSTEM\>".
