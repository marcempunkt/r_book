FROM rust:1 AS builder

WORKDIR /usr/src/book/
COPY ./ ./

RUN make install
RUN make build

FROM nginx:stable AS server

COPY --from=builder /usr/src/book/book/ /usr/share/nginx/html/

EXPOSE 80
