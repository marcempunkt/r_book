install:
	@cargo install mdbook@0.4.28
start:
	@mdbook serve
build:
	@mdbook build

docker_start:
	@docker run --name marc_rbuch --network host --rm -d marc/rbuch:latest
docker_build:
	@docker build -t marc/rbuch:latest .
