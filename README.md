**Dieses Buch ist immer noch "Work in Progress", 
aber eines Tages soll es "das perfekte R Buch für blutige Anfänger" werden.**

<div 
    style="display: flex; justify-content: center; align-items: center; width: 100%;"
>
    <img 
        src="/assets/mascot.png" 
        style="margin: auto; height: 300px; width: auto;" />
</div>

Dieses Buch ist für all diejenigen, die blutige Anfänger in Programmierung oder Data Science sind.
Bis zum Ende des Buches werden dir all diese Punkte näher gebracht:

- Programmieren
- Erstellung von hübschen und einfach verständlichen Statistiken
- 3D Grafen und Statistiken mit Animationen
- Verschiedene Methoden der Analyse von schriftlichen Werken

Falls du ein blutiger Anfänger sein solltest, 
dann lohnt es sich über das [Kapitel Grundlagen](grundlagen/variablen.md) zu gehen, 
sonst nutze dieses Buch gerne als Nachschlagewerk.

Ziel dieses Buches ist es, ein Verständnis dafür zu entwickeln, wie wir mit R Texte analysieren 
um wertvolle Erkenntnisse aus großen Textmengen zu extrahieren. 
Wir werden verschiedene Techniken und Methoden kennenlernen, die in der Textanalyse angewendet werden,
um Muster und Zusammenhänge zu erkennen.

### Warum R?

R wurde ursprünglich von Statistikern für statistische Analysen entwickelt und 
bleibt bis heute die bevorzugte Wahl vieler Statistiker. 

Während es nicht die beste Programmiersprache ist, um Systeme oder Applikationen zu erstellen, 
erleichtert die Syntax von R das Erstellen komplexer statistischer Modelle mit nur wenigen Codezeilen
und ist auch perfekt optimiert für den Data Science Bereich, außerdem kann man
viel erreichen ohne der beste Programmierer sein zu müssen.

### Warum nicht Python?

Pyhton gewinnt in den letzten Jahren mehr und mehr Popularität in Data Science.
Python ist eine "multi purpose language". 
"Multi purpose language" bedeuetet, dass man Python für viele verschiedene Usecases verwenden kann und 
es nicht für nur einen Bereich spezialisiert ist, im Gegensatz zu R, welches hauptsächlich
nur für Data Science gut geeignet ist.

Der Vorteil von Python ist, dass der Code sehr an der normalen englischen Sprache erinnert und
somit einfach verständlich ist, auch für diejenigen, die noch nie programmiert haben.

Dadurch dass man Python auch gut für Web- oder Systementwicklung verwenden kann,
lohnt es sich Python zu lernen. 
Vorallem gewinnt Python immer mehr an Popularität in der Data Science Community.

Der Vorteil von R ist, dass es speziell für Data Science entworfen worden ist.
Wenn du Data Science lernen möchtest und tiefer in die Materie einsteigen möchtests,
dann ist R perfekt, sonst würde ich schon fast empfehlen, für diejenigen, die keinen
guten Grund haben R zu benutzen und in mehr Themen als nur Data Science eintauchen möchten,
sich Python anzuschauen.

### License

Dieses Werk ist lizensiert unter [GNU Free Documentation License Version 1.3, 3 November 2008](https://www.gnu.org/licenses/fdl-1.3.html)
